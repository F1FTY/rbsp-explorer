# Part of the bsp-viewer project under the MIT license
# Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

CXX = g++
PKGCONFIG = $(shell which pkg-config)
CXXFLAGS = $(shell $(PKGCONFIG) --cflags glfw3) -I$(IMGUI_DIR) -I$(IMGUI_DIR)/backends --std=c++17 -Wall
LDLIBS = $(shell $(PKGCONFIG) --static --libs glfw3) -lGL -lGLEW
IMGUI_DIR = ./imgui
IMGUI = imgui/imgui*.cpp imgui/backends/imgui_impl_glfw.cpp imgui/backends/imgui_impl_opengl3.cpp



DUMMY != mkdir -p build


# Yes, I could generate object files
# No, I dont plan on doing so :)
SOURCES = \
	src/main.cpp \
	src/bsp.cpp \
	src/viewport/window_manager.cpp \
	src/viewport/viewport.cpp \
	src/games/games.cpp 


.PHONY: build
build: build/explorer

# g++ src/main.cpp src/window_manager.cpp -o build/explorer1 -lglfw
build/explorer: $(SOURCES)
	$(CXX) $(CXXFLAGS) $(SOURCES) -o $@ $(LDLIBS) $(IMGUI)
