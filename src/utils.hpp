// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

// Although this is called math.hpp it mainly contains helper structs

#pragma once

#include <string>


enum ePolygonMode {
	POLY_NONE,
	POLY_LINE,
	POLY_FILL
};


struct Vector3f {
	Vector3f() :
		x(0.0f), y(0.0f), z(0.0f) {}
	Vector3f( float _x, float _y, float _z ) :
		x(_x), y(_y), z(_z) {}

	float x;
	float y;
	float z;
};

struct Vector3h {
	int16_t x;
	int16_t y;
	int16_t z;
};

struct AABB {
	Vector3f mins;
	Vector3f maxs;
};

struct Lump_t {
	std::string name;
	bool canInspect;
	int coverage;
};
