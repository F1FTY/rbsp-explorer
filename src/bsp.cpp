// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#include "bsp.hpp"

/*
	Name: Bsp
	Purpose: Class constructor
*/
Bsp::Bsp( std::string path ) {
	std::ifstream file( path, std::ios::in | std::ios::binary );

	file.read((char*)&header, sizeof(header));

	if( strncmp( header.magic, "rBSP", 4 ) ) {
		printf( "Incorrect bsp magic! Expected: \"rBSP\"; Found: \"%s\"\n", header.magic );
		valid = false;
		file.close();
		return;
	}

	if( GetGameTypeFromVersion( header.version ) == UNSUPPORTED ) {
		printf( "Unsupported game / bsp version detected! Found %i\n", header.version );
		valid = false;
		file.close();
		return;
	}

	printf( "Successfuly opened bsp! magic: %.4s\n", header.magic );

	CopyLump( file, ENTITIES,			lumpEntities );
	CopyLump( file, VERTICES,			lumpVertices );
	CopyLump( file, VERTEX_NORMALS, 	lumpNormals );
	CopyLump( file, MESH_INDICES,		lumpMeshIndices );
	CopyLump( file, ENTITY_PARTITIONS,	lumpEntitiyPartitions );
	CopyLump( file, OCCLUSION_MESH_VERTICES,	lumpOcclusionMeshVertices );
	CopyLump( file, OCCLUSION_MESH_INDICES,		lumpOcclusionMeshIndices );
	CopyLump( file, OBJ_REFERENCE_BOUNDS,		lumpObjReferenceBounds );

	// Titanfall
	if ( GetGameTypeFromVersion( header.version ) != APEX_LEGENDS ) {
		CopyLump( file, MODELS,				lumpTitanfallModels );
		CopyLump( file, TEXTURE_DATA_STRING_DATA,		lumpSurfaceNames );
		CopyLump( file, VERTEX_LIT_BUMP,    lumpTitanfallVertexLitBump );
		CopyLump( file, MESHES,             lumpTitanfallMeshes );
		CopyLump( file, MATERIAL_SORT,      lumpTitanfallMaterialSort );
		CopyLump( file, TRICOLL_HEADERS,	lumpTitanfallTricollHeaders );
		CopyLump( file, CM_GRID,			lumpTitanfallCMGrid );
		CopyLump( file, CM_GRID_CELLS,		lumpTitanfallCMGridCells);
		CopyLump( file, CM_GEO_SETS,		lumpTitanfallCMGeoSets );
		CopyLump( file, CM_GEO_SET_BOUNDS,	lumpTitanfallCMGeoSetBounds );
		CopyLump( file, CM_PRIMITIVES,		lumpTitanfallCMPrimitives );
		CopyLump( file, CM_PRIMITIVE_BOUNDS,lumpTitanfallCMPrimitiveBounds );
		CopyLump( file, CM_BRUSHES,			lumpTitanfallCMBrushes );
		CopyLump( file, CM_BRUSH_SIDE_PLANE_OFFSETS, lumpTitanfallCMBrushSidePlaneOffsets );
		CopyLump( file, CM_BRUSH_SIDE_PROPERTIES, lumpTitanfallCMBrushSideProperties );
		CopyLump( file, CELL_AABB_NODES,	lumpTitanfallCellAABBNodes );
		CopyLump( file, OBJ_REFERENCES,		lumpTitanfallObjReferences);
		CopyLump( file, LEVEL_INFO,			lumpTitanfallLevelInfo );
	}
	// Apex Legends
	else {
		CopyLump( file, BVH_NODES,			lumpApexLegendsBVHNodes );
		CopyLump( file, PACKED_VERTICES,	lumpApexLegendsPackedVertices );
		CopyLump( file, MODELS,				lumpApexLegendsModels );
		CopyLump( file, SURFACE_NAMES,		lumpSurfaceNames );
		CopyLump( file, VERTEX_LIT_BUMP,    lumpApexLegendsVertexLitBump );
		CopyLump( file, MESHES,             lumpApexLegendsMeshes );
		CopyLump( file, MATERIAL_SORT,      lumpApexLegendsMaterialSort );
		CopyLump( file, CELL_AABB_NODES,	lumpApexLegendsCellAABBNodes );
		CopyLump( file, OBJ_REFERENCES,		lumpApexLegendsObjReferences );
		CopyLump( file, LEVEL_INFO,			lumpApexLegendsLevelInfo );
	}

	file.close();
}

/*
	Name: Bsp::CopyLump
	Purpose: Copies lump from file into a vector
*/
template<typename T>
void Bsp::CopyLump( std::ifstream &file, int lump, std::vector<T> &data ) {
	if ( header.lumps[lump].length % sizeof(T) != 0 ) {
		printf( "Length mismatch! Lump: %i\n", lump );
		return;
	}
	
	data.clear();

	file.seekg( header.lumps[lump].offset );

	data.resize( header.lumps[lump].length / sizeof(T) );
	file.read( (char*)&data[0], header.lumps[lump].length );
}

/*
	Name: Bsp::GetDrawableMeshes
	Purpose: Builds a drawable using bsp meshes
*/
Drawable Bsp::GetDrawableMeshes() {
	std::vector<DrawableVertex_t> vertices;
	std::vector<uint32_t> indices;
	std::vector<Mesh_t> meshes;

	Drawable drawable;
	
	// Titanfall
	if ( GetGameTypeFromVersion( header.version ) != APEX_LEGENDS ) {
		//printf( "Building drawable from Titanfall meshes!\n" );

		for( std::size_t index = 0; index < lumpTitanfallVertexLitBump.size(); index++ ) {
			DrawableVertex_t vertex;
			vertex.position = lumpVertices[ lumpTitanfallVertexLitBump[ index ].vertexIndex ];
			vertex.normal = lumpNormals[ lumpTitanfallVertexLitBump[ index ].normalIndex ];
			vertices.push_back( vertex );
		}


		// Loop through meshes
		for( Titanfall::Mesh_t &mesh : lumpTitanfallMeshes ) {
			if( ( mesh.flags & 0x200 ) == 0 ) {
				// This makes sure lumpMeshes and Drawable.meshes are parallel even when we only support litVertexBump
				// TODO: Add support for the rest of vertex types
				Mesh_t m;
				m.start = 0;
				m.end = 0;
				meshes.push_back( m );
				continue;
			}

			Mesh_t m;
			m.start = indices.size();
			for( uint32_t index = 0; index < mesh.triangleCount * 3; index++ ) {
				indices.push_back( lumpMeshIndices[ mesh.triangleOffset + index ] + lumpTitanfallMaterialSort[ mesh.materialOffset ].vertexOffset );
			}
			m.end = indices.size();
			meshes.push_back( m );
		}


		drawable.vertexCount = vertices.size();
		drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
		for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
			drawable.vertices[i] = vertices[ i ];
		}

		drawable.indexCount = indices.size();
		drawable.indices = new uint32_t[ drawable.indexCount ];
		for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
			drawable.indices[i] = indices[ i ];
		}

		drawable.meshes = meshes;
	}
	// Apex Legends
	else {
		//printf( "Building drawable from Apex Legends meshes!\n" );

		for( std::size_t index = 0; index < lumpApexLegendsVertexLitBump.size(); index++ ) {
			DrawableVertex_t vertex;
			vertex.position = lumpVertices[ lumpApexLegendsVertexLitBump[ index ].vertexIndex ];
			vertex.normal = lumpNormals[ lumpApexLegendsVertexLitBump[ index ].normalIndex ];
			vertices.push_back( vertex );
		}

		// Loop through meshes
		for( ApexLegends::Mesh_t &mesh : lumpApexLegendsMeshes ) {
			if( ( mesh.flags & 0x200 ) == 0 ) {
				// This makes sure lumpMeshes and Drawable.meshes are parallel even when we only support litVertexBump
				// TODO: Add support for the rest of vertex types
				Mesh_t m;
				m.start = 0;
				m.end = 0;
				meshes.push_back( m );
				continue;
			}

			Mesh_t m;
			m.start = indices.size();
			for( uint32_t index = 0; index < mesh.triangleCount * 3; index++ ) {
				indices.push_back( lumpMeshIndices[ mesh.triangleOffset + index ] + lumpApexLegendsMaterialSort[ mesh.materialOffset ].vertexOffset );
			}
			m.end = indices.size();
			meshes.push_back( m );
		}


		drawable.vertexCount = vertices.size();
		drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
		for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
			drawable.vertices[i] = vertices[ i ];
		}

		drawable.indexCount = indices.size();
		drawable.indices = new uint32_t[ drawable.indexCount ];
		for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
			drawable.indices[i] = indices[ i ];
		}

		drawable.meshes = meshes;
	}

	//printf( "Drawable meshes: %li\n", drawable.meshes.size() );

	return drawable;
}

/*
	Name: Bsp::GetDrawableModels
	Purpose: Builds a drawable using bsp models
*/
Drawable Bsp::GetDrawableModels() {
	std::vector<DrawableVertex_t> vertices;
	std::vector<uint32_t> indices;
	std::vector<Mesh_t> meshes;

	Drawable drawable;

	// Both titanfall and apex models start with an AABB
	// So we might as well not duplicate the Drawable gen code
	std::vector<AABB> aabbs;

	// Titanfall
	if ( GetGameTypeFromVersion( header.version ) != APEX_LEGENDS ) {
		//printf( "Building drawable from Titanfall models!\n" );
		for ( Titanfall::Model_t &model : lumpTitanfallModels )
			aabbs.emplace_back( model.aabb );
	}
	// Apex Legends
	else {
		//printf( "Building drawable from Apex Legends models!\n" );
		for ( ApexLegends::Model_t &model : lumpApexLegendsModels )
			aabbs.emplace_back( model.aabb );
	}

	// Build the drawable itself
	for ( AABB &aabb : aabbs ) {
		uint32_t indexOffset = (uint32_t)vertices.size();
		Mesh_t m;
		m.start = indices.size();

		std::vector<DrawableVertex_t> aabbVertices;
		aabbVertices = drawable.GetDrawableVerticesFromAABB( aabb );
		vertices.insert( vertices.end(), aabbVertices.begin(), aabbVertices.end() );

		std::vector<uint32_t> aabbIndices;
		aabbIndices = drawable.GetIndicesFromAABB( aabb );
		for ( uint32_t &index : aabbIndices )
			indices.push_back( index + indexOffset );

		m.end = indices.size();
		meshes.push_back( m );
	}

	drawable.vertexCount = vertices.size();
	drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
	for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
		drawable.vertices[i] = vertices[ i ];
	}

	drawable.indexCount = indices.size();
	drawable.indices = new uint32_t[ drawable.indexCount ];
	for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
		drawable.indices[i] = indices[ i ];
	}

	drawable.meshes = meshes;


	//printf( "Drawable meshes: %li\n", drawable.meshes.size() );

	return drawable;
}

/*
	Name: Bsp::GetDrawableCellAABBNodes
	Purpose: Builds a drawable using Cell AABB Nodes
*/
Drawable Bsp::GetDrawableCellAABBNodes() {
	std::vector<DrawableVertex_t> vertices;
	std::vector<uint32_t> indices;
	std::vector<Mesh_t> meshes;

	Drawable drawable;

	// Both titanfall and apex models start with an AABB
	// So we might as well not duplicate the Drawable gen code
	std::vector<AABB> aabbs;

	// Titanfall
	if ( GetGameTypeFromVersion( header.version ) != APEX_LEGENDS ) {
		//printf( "Building drawable from Titanfall Cell AABB Nodes!\n" );
		for ( Titanfall::CellAABBNode_t &cell : lumpTitanfallCellAABBNodes ) {
			AABB aabb;
			aabb.mins = cell.mins;
			aabb.maxs = cell.maxs;
			aabbs.emplace_back( aabb );
		}
	}
	// Apex Legends
	else {
		//printf( "Building drawable from Apex Legends Cell AABB Nodes!\n" );
		for ( ApexLegends::CellAABBNode_t &cell : lumpApexLegendsCellAABBNodes ) {
			AABB aabb;
			aabb.mins = cell.mins;
			aabb.maxs = cell.maxs;
			aabbs.emplace_back( aabb );
		}
	}

	// Build the drawable itself
	for ( AABB &aabb : aabbs ) {
		uint32_t indexOffset = (uint32_t)vertices.size();
		Mesh_t m;
		m.start = indices.size();

		std::vector<DrawableVertex_t> aabbVertices;
		aabbVertices = drawable.GetDrawableVerticesFromAABB( aabb );
		vertices.insert( vertices.end(), aabbVertices.begin(), aabbVertices.end() );

		std::vector<uint32_t> aabbIndices;
		aabbIndices = drawable.GetIndicesFromAABB( aabb );
		for ( uint32_t &index : aabbIndices )
			indices.push_back( index + indexOffset );

		m.end = indices.size();
		meshes.push_back( m );
	}

	drawable.vertexCount = vertices.size();
	drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
	for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
		drawable.vertices[i] = vertices[ i ];
	}

	drawable.indexCount = indices.size();
	drawable.indices = new uint32_t[ drawable.indexCount ];
	for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
		drawable.indices[i] = indices[ i ];
	}

	drawable.meshes = meshes;


	//printf( "Drawable meshes: %li\n", drawable.meshes.size() );

	return drawable;
}

/*
	Name: Bsp::GetDrawableOcclusionMesh
	Purpose: Builds a drawable using Cell AABB Nodes
*/
Drawable Bsp::GetDrawableOcclusionMesh() {
	std::vector<DrawableVertex_t> vertices;
	std::vector<Mesh_t> meshes;
	Mesh_t m;
	m.start = 0;
	m.end = lumpOcclusionMeshIndices.size();
	meshes.push_back( m );

	for ( Vector3f &vertex : lumpOcclusionMeshVertices ) {
		DrawableVertex_t v;
		v.position = vertex;
		vertices.push_back( v );
	}

	Drawable drawable;
	
	// Build drawable
	drawable.vertexCount = vertices.size();
	drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
	for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
		drawable.vertices[i] = vertices[ i ];
	}

	drawable.indexCount = lumpOcclusionMeshIndices.size();
	drawable.indices = new uint32_t[ drawable.indexCount ];
	for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
		drawable.indices[i] = lumpOcclusionMeshIndices[ i ];
	}

	drawable.meshes = meshes;


	//printf( "Drawable meshes: %li\n", drawable.meshes.size() );

	return drawable;
}

/*
	Name: Bsp::GetDrawableCMBounds
	Purpose: Builds a drawable using either CM_GEO_SET_BOUNDS or CM_PRIMITIVE_BOUNDS
*/
Drawable Bsp::GetDrawableCMBounds( int lump ) {
	std::vector<AABB> aabbs;
	switch ( lump ) {
		case CM_GEO_SET_BOUNDS:
			for ( Titanfall::CMBounds_t &bound : lumpTitanfallCMGeoSetBounds ) {
				AABB aabb;
				aabb.mins = Vector3f( bound.origin.x - bound.extents.x, bound.origin.y - bound.extents.y, bound.origin.z - bound.extents.z );
				aabb.maxs = Vector3f( bound.origin.x + bound.extents.x, bound.origin.y + bound.extents.y, bound.origin.z + bound.extents.z );
				aabbs.emplace_back( aabb );
			}
			break;
		case CM_PRIMITIVE_BOUNDS:
			for ( Titanfall::CMBounds_t &bound : lumpTitanfallCMPrimitiveBounds ) {
				AABB aabb;
				aabb.mins = Vector3f( bound.origin.x - bound.extents.x, bound.origin.y - bound.extents.y, bound.origin.z - bound.extents.z );
				aabb.maxs = Vector3f( bound.origin.x + bound.extents.x, bound.origin.y + bound.extents.y, bound.origin.z + bound.extents.z );
				aabbs.emplace_back( aabb );
			}
			break;
	}

	std::vector<DrawableVertex_t> vertices;
	std::vector<uint32_t> indices;
	std::vector<Mesh_t> meshes;

	Drawable drawable;

	// Build the drawable itself
	for ( AABB &aabb : aabbs ) {
		uint32_t indexOffset = (uint32_t)vertices.size();
		Mesh_t m;
		m.start = indices.size();

		std::vector<DrawableVertex_t> aabbVertices;
		aabbVertices = drawable.GetDrawableVerticesFromAABB( aabb );
		vertices.insert( vertices.end(), aabbVertices.begin(), aabbVertices.end() );

		std::vector<uint32_t> aabbIndices;
		aabbIndices = drawable.GetIndicesFromAABB( aabb );
		for ( uint32_t &index : aabbIndices )
			indices.push_back( index + indexOffset );

		m.end = indices.size();
		meshes.push_back( m );
	}

	drawable.vertexCount = vertices.size();
	drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
	for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
		drawable.vertices[i] = vertices[ i ];
	}

	drawable.indexCount = indices.size();
	drawable.indices = new uint32_t[ drawable.indexCount ];
	for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
		drawable.indices[i] = indices[ i ];
	}

	drawable.meshes = meshes;

	//printf( "Meshes: %li\n", meshes.size() );

	return drawable;
}

/*
	Name: Bsp::GetDrawableCMBrushes
	Purpose: Builds a drawable using CM_BRUSHES
*/
Drawable Bsp::GetDrawableCMBrushes() {
	// TODO: Add support for plane cutting
	std::vector<AABB> aabbs;
	for ( Titanfall::CMBrush_t &brush : lumpTitanfallCMBrushes ) {
		AABB aabb;
		aabb.mins = Vector3f( brush.origin.x - brush.extents.x, brush.origin.y - brush.extents.y, brush.origin.z - brush.extents.z );
		aabb.maxs = Vector3f( brush.origin.x + brush.extents.x, brush.origin.y + brush.extents.y, brush.origin.z + brush.extents.z );
		aabbs.emplace_back( aabb );
	}

	std::vector<DrawableVertex_t> vertices;
	std::vector<uint32_t> indices;
	std::vector<Mesh_t> meshes;

	Drawable drawable;

	// Build the drawable itself
	for ( AABB &aabb : aabbs ) {
		uint32_t indexOffset = (uint32_t)vertices.size();
		Mesh_t m;
		m.start = indices.size();

		std::vector<DrawableVertex_t> aabbVertices;
		aabbVertices = drawable.GetDrawableVerticesFromAABB( aabb );
		vertices.insert( vertices.end(), aabbVertices.begin(), aabbVertices.end() );

		std::vector<uint32_t> aabbIndices;
		aabbIndices = drawable.GetIndicesFromAABB( aabb );
		for ( uint32_t &index : aabbIndices )
			indices.push_back( index + indexOffset );

		m.end = indices.size();
		meshes.push_back( m );
	}

	drawable.vertexCount = vertices.size();
	drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
	for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
		drawable.vertices[i] = vertices[ i ];
	}

	drawable.indexCount = indices.size();
	drawable.indices = new uint32_t[ drawable.indexCount ];
	for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
		drawable.indices[i] = indices[ i ];
	}

	drawable.meshes = meshes;

	//printf( "Meshes: %li\n", meshes.size() );

	return drawable;
}

/*
	Name: Bsp::GetDrawableObjReferenceBounds
	Purpose: Builds a drawable using OBJ_REFERENCE_BOUNDS
*/
Drawable Bsp::GetDrawableObjReferenceBounds() {
	std::vector<AABB> aabbs;
	for ( Titanfall::ObjReferenceBounds_t &bound : lumpObjReferenceBounds ) {
		AABB aabb;
		aabb.mins = bound.mins;
		aabb.maxs = bound.maxs;
		aabbs.emplace_back( aabb );
	}

	std::vector<DrawableVertex_t> vertices;
	std::vector<uint32_t> indices;
	std::vector<Mesh_t> meshes;

	Drawable drawable;

	// Build the drawable itself
	for ( AABB &aabb : aabbs ) {
		uint32_t indexOffset = (uint32_t)vertices.size();
		Mesh_t m;
		m.start = indices.size();

		std::vector<DrawableVertex_t> aabbVertices;
		aabbVertices = drawable.GetDrawableVerticesFromAABB( aabb );
		vertices.insert( vertices.end(), aabbVertices.begin(), aabbVertices.end() );

		std::vector<uint32_t> aabbIndices;
		aabbIndices = drawable.GetIndicesFromAABB( aabb );
		for ( uint32_t &index : aabbIndices )
			indices.push_back( index + indexOffset );

		m.end = indices.size();
		meshes.push_back( m );
	}

	drawable.vertexCount = vertices.size();
	drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
	for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
		drawable.vertices[i] = vertices[ i ];
	}

	drawable.indexCount = indices.size();
	drawable.indices = new uint32_t[ drawable.indexCount ];
	for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
		drawable.indices[i] = indices[ i ];
	}

	drawable.meshes = meshes;

	//printf( "Meshes: %li\n", meshes.size() );

	return drawable;
}

/*
	Name: Bsp::GetDrawableTricoll
	Purpose: Builds a point cloud using TRICOLL_HEADERS
*/
Drawable Bsp::GetDrawableTricoll() {
	Drawable drawable;

	std::vector<DrawableVertex_t> vertices;
	std::vector<uint32_t> indices;
	std::vector<Mesh_t> meshes;

	// Build pointcloud
	for( Titanfall::TricollHeader_t &header : lumpTitanfallTricollHeaders ) {
		Mesh_t mesh;
		mesh.start = indices.size();
		for( uint32_t i = header.vertexOffset; i < header.vertexCount; i++ ) {
			Vector3f &vertex = lumpVertices[i];
			DrawableVertex_t v;
			v.position = Vector3f( 0.5 + vertex.x, 0.5 + vertex.y, 0 + vertex.z );
			vertices.push_back( v );
			v.position = Vector3f( 0.5 + vertex.x, -0.5 + vertex.y, 0 + vertex.z );
			vertices.push_back( v );
			v.position = Vector3f( -0.5 + vertex.x, 0 + vertex.y, 0 + vertex.z );
			vertices.push_back( v );
			v.position = Vector3f( 0 + vertex.x, 0 + vertex.y, 1 + vertex.z );
			vertices.push_back( v );

			indices.push_back( 0 + i * 4 );
			indices.push_back( 1 + i * 4 );
			indices.push_back( 2 + i * 4 );

			indices.push_back( 0 + i * 4 );
			indices.push_back( 1 + i * 4 );
			indices.push_back( 3 + i * 4 );

			indices.push_back( 1 + i * 4 );
			indices.push_back( 2 + i * 4 );
			indices.push_back( 3 + i * 4 );

			indices.push_back( 2 + i * 4 );
			indices.push_back( 0 + i * 4 );
			indices.push_back( 3 + i * 4 );
		}
		mesh.end = indices.size();
		meshes.push_back( mesh );
	}
	

	// Fill drawable
	drawable.vertexCount = vertices.size();
	drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
	for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
		drawable.vertices[i] = vertices[ i ];
	}

	drawable.indexCount = indices.size();
	drawable.indices = new uint32_t[ drawable.indexCount ];
	for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
		drawable.indices[i] = indices[ i ];
	}

	drawable.meshes = meshes;

	printf( "Made: %li meshes\n", drawable.meshes.size() );

	return drawable;
}

/*
	Name: Bsp::GetDrawableBVHNodes
	Purpose: Builds a drawable using BVH_NODES
*/
Drawable Bsp::GetDrawableBVHNodes() {
	std::vector<AABB> aabbs;
	for ( ApexLegends::BVHNode_t &node : lumpApexLegendsBVHNodes ) {
		AABB aabb;
		aabb.mins = Vector3f( node.mins0.x, node.mins1.y, node.mins3.z );
		aabb.maxs = Vector3f( node.maxs0.x, node.maxs1.y, node.maxs3.z );
		aabbs.emplace_back( aabb );


		//aabb.mins = Vector3f( node.mins1.x, node.mins1.y, node.mins1.z );
		//aabb.maxs = Vector3f( node.maxs1.x, node.maxs1.y, node.maxs1.z );
		//aabbs.emplace_back( aabb );

		aabb.mins = Vector3f( node.mins2.x, node.mins2.y, node.mins2.z );
		aabb.maxs = Vector3f( node.maxs2.x, node.maxs2.y, node.maxs2.z );
		//aabbs.emplace_back( aabb );

		aabb.mins = Vector3f( node.mins3.x, node.mins3.y, node.mins3.z );
		aabb.maxs = Vector3f( node.maxs3.x, node.maxs3.y, node.maxs3.z );
		//aabbs.emplace_back( aabb );
	}

	std::vector<DrawableVertex_t> vertices;
	std::vector<uint32_t> indices;
	std::vector<Mesh_t> meshes;

	Drawable drawable;

	// Build the drawable itself
	for ( AABB &aabb : aabbs ) {
		uint32_t indexOffset = (uint32_t)vertices.size();
		Mesh_t m;
		m.start = indices.size();

		std::vector<DrawableVertex_t> aabbVertices;
		aabbVertices = drawable.GetDrawableVerticesFromAABB( aabb );
		vertices.insert( vertices.end(), aabbVertices.begin(), aabbVertices.end() );

		std::vector<uint32_t> aabbIndices;
		aabbIndices = drawable.GetIndicesFromAABB( aabb );
		for ( uint32_t &index : aabbIndices )
			indices.push_back( index + indexOffset );

		m.end = indices.size();
		meshes.push_back( m );
	}

	drawable.vertexCount = vertices.size();
	drawable.vertices = new DrawableVertex_t[ drawable.vertexCount ];
	for( uint32_t i = 0; i < drawable.vertexCount; i++ ) {
		drawable.vertices[i] = vertices[ i ];
	}

	drawable.indexCount = indices.size();
	drawable.indices = new uint32_t[ drawable.indexCount ];
	for( uint32_t i = 0; i < drawable.indexCount; i++ ) {
		drawable.indices[i] = indices[ i ];
	}

	drawable.meshes = meshes;

	//printf( "Meshes: %li\n", meshes.size() );

	return drawable;
}