// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#pragma once

#include "viewport/drawable.hpp"
#include "games/games.hpp"
#include "utils.hpp"
#include <string>
#include <cstring>
#include <fstream>
#include <vector>

struct LumpEntry_t {
	uint32_t offset;
	uint32_t length;
	uint32_t version;
	uint32_t compressedLength;
};

struct rBSPHeader_t {
	char magic[4];			// rBSP
	uint32_t version;		// Bsp version
	uint32_t mapVersion;	// Idk, saw this somewhere ( or made it up )
	uint32_t lumpMax;		// 127

	LumpEntry_t lumps[128];
};

/*
	Class: Bsp
	Purpose: Stores the bsp header and loaded lumps
*/
class Bsp {
	public:
	rBSPHeader_t header;
	bool valid = true;

	std::vector<char> lumpEntities;
	std::vector<Vector3f> lumpVertices;
	std::vector<char> lumpSurfaceNames; // Used for both games, just different index
	std::vector<Vector3f> lumpNormals;
	std::vector<uint16_t> lumpMeshIndices;
	std::vector<Vector3f> lumpOcclusionMeshVertices;
	std::vector<uint16_t> lumpOcclusionMeshIndices;
	std::vector<char> lumpEntitiyPartitions;
	std::vector<Titanfall::ObjReferenceBounds_t> lumpObjReferenceBounds;
	// Titanfall specific
	std::vector<Titanfall::Model_t> lumpTitanfallModels;
	std::vector<Titanfall::VertexLitBump_t> lumpTitanfallVertexLitBump;
	std::vector<Titanfall::Mesh_t> lumpTitanfallMeshes;
	std::vector<Titanfall::MaterialSort_t> lumpTitanfallMaterialSort;
	std::vector<Titanfall::TricollHeader_t> lumpTitanfallTricollHeaders;
	std::vector<Titanfall::CMGrid_t> lumpTitanfallCMGrid;
	std::vector<Titanfall::CMGridCell_t> lumpTitanfallCMGridCells;
	std::vector<Titanfall::CMGeoSet_t> lumpTitanfallCMGeoSets;
	std::vector<Titanfall::CMBounds_t> lumpTitanfallCMGeoSetBounds;
	std::vector<Titanfall::CMPrimitive_t> lumpTitanfallCMPrimitives;
	std::vector<Titanfall::CMBounds_t> lumpTitanfallCMPrimitiveBounds;
	std::vector<Titanfall::CMBrush_t> lumpTitanfallCMBrushes;
	std::vector<uint16_t> lumpTitanfallCMBrushSidePlaneOffsets;
	std::vector<uint16_t> lumpTitanfallCMBrushSideProperties;
	std::vector<Titanfall::CellAABBNode_t> lumpTitanfallCellAABBNodes;
	std::vector<uint16_t> lumpTitanfallObjReferences;
	std::vector<Titanfall::LevelInfo_t> lumpTitanfallLevelInfo;
	// Apex Legends specific
	std::vector<ApexLegends::BVHNode_t> lumpApexLegendsBVHNodes;
	std::vector<Vector3h> lumpApexLegendsPackedVertices;
	std::vector<ApexLegends::Model_t> lumpApexLegendsModels;
	std::vector<ApexLegends::VertexLitBump_t> lumpApexLegendsVertexLitBump;
	std::vector<ApexLegends::Mesh_t> lumpApexLegendsMeshes;
	std::vector<ApexLegends::MaterialSort_t> lumpApexLegendsMaterialSort;
	std::vector<ApexLegends::CellAABBNode_t> lumpApexLegendsCellAABBNodes;
	std::vector<uint32_t> lumpApexLegendsObjReferences;
	std::vector<ApexLegends::LevelInfo_t> lumpApexLegendsLevelInfo;

	private:
	template<typename T>
	void CopyLump( std::ifstream &file, int lump, std::vector<T> &data );

	public:
	Bsp() {};
	Bsp( std::string path );

	Drawable GetDrawableMeshes();
	Drawable GetDrawableModels();
	Drawable GetDrawableCellAABBNodes();
	Drawable GetDrawableOcclusionMesh();
	Drawable GetDrawableCMBounds( int lump );
	Drawable GetDrawableCMBrushes();
	Drawable GetDrawableObjReferenceBounds();
	Drawable GetDrawableTricoll();
	Drawable GetDrawableBVHNodes();
};