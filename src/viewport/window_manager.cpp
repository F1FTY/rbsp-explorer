// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#include "window_manager.hpp"

/*
	Name: WindowManager::WindowManager
	Purpose: Class constructor
*/
WindowManager::WindowManager( GLFWwindow *window, Viewport &view, Bsp &map ) {
	// I'm unsure if this is best practise
	// I know both of these will exist until the program stops so it should be fine ??
	// TODO: Maybe ask someone who knows more about cpp
	bsp = &map;
	viewport = &view;
	viewport->polygonMode = &polygonMode;

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;

	ImGui::StyleColorsDark();

	ImGui_ImplGlfw_InitForOpenGL( window, true );
	ImGui_ImplOpenGL3_Init( "#version 130" );

	for ( int i = 0; i < 128; i++ )
		windows[i] = false;
	
	// Initial value of `type` is MESHES so no need to set it this time
	viewport->SetDrawable( bsp->GetDrawableMeshes() );
}

/*
	Name: WindowManager::Update
	Purpose: Refreshes ImGui windows
*/
void WindowManager::Update() {
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	Window_BspInfo();
	Window_ViewportInfo();
	UpdateLumpWindows();

	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData( ImGui::GetDrawData() );
}

/*
	Name: WindowManager::Window_BspInfo
	Purpose: Draws the "BSP Info" ImGui window
*/
void WindowManager::Window_BspInfo() {
	ImGui::Begin( "BSP Info" );

	if ( ImGui::IsWindowCollapsed() ) {
		ImGui::End();
		return;
	}

	// Although this could be set at startup once as a global
	// I may want to implement basic .bsp editing which is why it's here
	int game = GetGameTypeFromVersion( bsp->header.version );

	if ( game == UNSUPPORTED ) {
		ImGui::Text( "The file you have opened either doesn't exist or isn't a valid respawn bsp!" );

		ImGui::End();
		return;
	}

	ImGui::Text( "Header.version: %i", bsp->header.version );
	ImGui::Text( "Game: %s", GetGameNameFromVersion( bsp->header.version ).c_str() );

	ImGui::BeginTable( "Lumps", 5, ImGuiTableFlags_Resizable | ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg );
	ImGui::TableSetupColumn( "Index" );
	ImGui::TableSetupColumn( "Name" );
	ImGui::TableSetupColumn( "Size" );
	ImGui::TableSetupColumn( "Version" );
	ImGui::TableSetupColumn( "Inspect" );
	ImGui::TableHeadersRow();

	for ( int i = 0; i < 128; i++ ) {
		if ( !IsLumpUsedInGame( i, game ) )
			continue;

		Lump_t lump = GetLumpInfo( i, game );
		
		ImGui::TableNextColumn();
		ImGui::Text( "%i (0x%x)", i, i );
		ImGui::TableNextColumn();
		ImGui::Text( "%s", lump.name.c_str() );
		ImGui::TableNextColumn();
		ImGui::Text( "%i", bsp->header.lumps[i].length );
		ImGui::TableNextColumn();
		ImGui::Text( "%i", bsp->header.lumps[i].version );
		ImGui::TableNextColumn();
		// Inspect button
		ImGui::PushID(i);
		if ( !lump.canInspect )
			ImGui::BeginDisabled();
		
		if ( ImGui::Button( "Inspect" ) ) {
			windows[i] = true;
		}
		if ( !lump.canInspect )
			ImGui::EndDisabled();
		
		ImGui::PopID();

		ImGui::TableNextRow();
	}

	ImGui::EndTable();

	ImGui::End();
}

/*
	Name WindowManager::Window_ViewportInfo
	Purpose: Draws the "Viewport Info" window
*/
void WindowManager::Window_ViewportInfo() {
	ImGui::Begin( "Viewport Info" );

	static bool cullBackFace = true;
	ImGui::Checkbox( "Cull Back Faces", &cullBackFace );

	static int lastPolygonMode = -1;
	ImGui::SameLine( 200 );
	ImGui::RadioButton( "Don't force polygon mode", &polygonMode, POLY_NONE );
	
	ImGui::Button( "Show all" );
	ImGui::SameLine( 200 );
	ImGui::RadioButton( "Force fill faces", &polygonMode, POLY_FILL );
	
	if ( ImGui::Button( "Reset highlighted meshes" ) ) {
		viewport->ResetHighlight();
	}

	ImGui::SameLine( 200 );
	ImGui::RadioButton( "Force wireframe faces", &polygonMode, POLY_LINE );

	if ( polygonMode != lastPolygonMode ) {
		switch ( polygonMode ) {
			case POLY_FILL:
				glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
				break;
			case POLY_LINE:
				glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
				break;
		}

		lastPolygonMode = polygonMode;
	}
	

	if ( cullBackFace )
		glEnable( GL_CULL_FACE );
	else
		glDisable( GL_CULL_FACE );



	static int lastType = MESHES;

	ImGui::Separator();
	ImGui::RadioButton( "Meshes", &type, MESHES );
	ImGui::RadioButton( "Models", &type, MODELS );
	ImGui::RadioButton( "Cell AABB Nodes", &type, CELL_AABB_NODES );
	ImGui::RadioButton( "Occlusion mesh", &type, OCCLUSION_MESH_VERTICES );
	ImGui::RadioButton( "Obj Reference Bounds", &type, OBJ_REFERENCE_BOUNDS );
	if( GetGameTypeFromVersion( bsp->header.version ) != APEX_LEGENDS ) {
		ImGui::RadioButton( "Geo Set Bounds", &type, CM_GEO_SET_BOUNDS );
		ImGui::RadioButton( "Primitive Bounds", &type, CM_PRIMITIVE_BOUNDS );
		ImGui::RadioButton( "Brushes", &type, CM_BRUSHES );
		ImGui::RadioButton( "Tricoll", &type, TRICOLL_HEADERS );
	}
	else {
		ImGui::RadioButton( "Collision BVH Nodes", &type, BVH_NODES );
	}

	ImGui::End();

	// TODO: Maybe instead of building the drawable each time we change to a different one
	// we should just build them once and then save them somewhere

	if ( lastType != type ) {
		// printf( "Viewport type changed to lump: %i ( 0x%x )!\n", type, type );
		switch ( type ) {
			case MESHES:
				viewport->SetDrawable( bsp->GetDrawableMeshes() );
				break;
			case MODELS:
				viewport->SetDrawable( bsp->GetDrawableModels() );
				break;
			case CELL_AABB_NODES:
				viewport->SetDrawable( bsp->GetDrawableCellAABBNodes() );
				break;
			case OCCLUSION_MESH_VERTICES:
				viewport->SetDrawable( bsp->GetDrawableOcclusionMesh() );
				break;
			case CM_GEO_SET_BOUNDS:
				viewport->SetDrawable( bsp->GetDrawableCMBounds( CM_GEO_SET_BOUNDS ) );
				break;
			case CM_PRIMITIVE_BOUNDS:
				viewport->SetDrawable( bsp->GetDrawableCMBounds( CM_PRIMITIVE_BOUNDS ) );
				break;
			case CM_BRUSHES:
				viewport->SetDrawable( bsp->GetDrawableCMBrushes() );
				break;
			case OBJ_REFERENCE_BOUNDS:
				viewport->SetDrawable( bsp->GetDrawableObjReferenceBounds() );
				break;
			case TRICOLL_HEADERS:
				viewport->SetDrawable( bsp->GetDrawableTricoll() );
				break;
			case BVH_NODES:
				viewport->SetDrawable( bsp->GetDrawableBVHNodes() );
		};
	}

	lastType = type;
}

/*
	Name: WindowManager::Window_Models
	Purpose: Draws the "Models" window
*/
void WindowManager::Window_Models() {
	if( GetGameTypeFromVersion( bsp->header.version ) != APEX_LEGENDS )
		ImGui::Text( "Models: %li", bsp->lumpTitanfallModels.size() );
	else
		ImGui::Text( "Models: %li", bsp->lumpApexLegendsModels.size() );


	ImGui::BeginTable( "Models", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg );
	ImGui::TableSetupColumn( "Index" );
	ImGui::TableSetupColumn( "Value" );
	ImGui::TableHeadersRow();

	// Titanfall
	if( GetGameTypeFromVersion( bsp->header.version ) != APEX_LEGENDS ) {
		for ( std::size_t i = 0; i < bsp->lumpTitanfallModels.size(); i++ ) {
			Titanfall::Model_t &model = bsp->lumpTitanfallModels[i];

			ImGui::TableNextColumn();
			std::string index = std::to_string( i );
			if ( ImGui::TreeNode( index.c_str() ) ){
				ImGui::TableNextRow();

				ImGui::TableNextColumn();
				ImGui::Text("    mins");
				ImGui::TableNextColumn();
				ImGui::Text( "%f, %f, %f", model.aabb.mins.x, model.aabb.mins.y, model.aabb.mins.z );
				
				ImGui::TableNextColumn();
				ImGui::Text( "    maxs" );
				ImGui::TableNextColumn();
				ImGui::Text( "%f, %f, %f", model.aabb.maxs.x, model.aabb.maxs.y, model.aabb.maxs.z );

				ImGui::TableNextColumn();
				ImGui::Text( "    firstMesh" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", model.firstMesh );

				ImGui::TableNextColumn();
				ImGui::Text( "    meshCount" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", model.meshCount );

				ImGui::TreePop();
			}

			ImGui::TableNextRow();
		}
	}
	// Apex Legends
	else {
		for ( std::size_t i = 0; i < bsp->lumpApexLegendsModels.size(); i++ ) {
			ApexLegends::Model_t &model = bsp->lumpApexLegendsModels[i];

			ImGui::TableNextColumn();
			std::string index = std::to_string( i );
			if ( ImGui::TreeNode( index.c_str() ) ){
				ImGui::TableNextRow();

				ImGui::TableNextColumn();
				ImGui::Text("    mins");
				ImGui::TableNextColumn();
				ImGui::Text( "%f, %f, %f", model.aabb.mins.x, model.aabb.mins.y, model.aabb.mins.z );
				
				ImGui::TableNextColumn();
				ImGui::Text( "    maxs" );
				ImGui::TableNextColumn();
				ImGui::Text( "%f, %f, %f", model.aabb.maxs.x, model.aabb.maxs.y, model.aabb.maxs.z );

				ImGui::TableNextColumn();
				ImGui::Text( "    firstMesh" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", model.firstMesh );

				ImGui::TableNextColumn();
				ImGui::Text( "    meshCount" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", model.meshCount );

				for ( int j = 0; j < 8; j++ ) {
					ImGui::TableNextColumn();
					ImGui::Text( "    unknown %i", j );
					ImGui::TableNextColumn();
					ImGui::Text( "%i", model.unknown[j] );
				}

				ImGui::TreePop();
			}

			ImGui::TableNextRow();
		}
	}

	ImGui::EndTable();
}

/*
	Name: WindowManager::Window_SurfaceNames
	Purpose: Draws the "Surface Names" or "Texture Data String Data" windows
*/
void WindowManager::Window_SurfaceNames() {
	static std::string buffer;
	static std::size_t count = 0;

	ImGui::InputText( "Search", buffer.data(), 64 );
	ImGui::Text( "Count: %li", count );
	ImGui::Separator();

	std::string textures = { bsp->lumpSurfaceNames.begin(), bsp->lumpSurfaceNames.end() };
	std::stringstream ss( textures );

	count = 0;
	std::string line;
	while ( std::getline( ss, line, '\0' ) ) {
		if ( line.find( buffer.c_str() ) != std::string::npos ) {
			ImGui::Text( line.c_str() );
			count++;
		}
	}
}

/*
	Name: WindowManager::Window_Entities
	Purpose: Draws the "Entities" window
*/
void WindowManager::Window_Entities() {
	std::string entities = { bsp->lumpEntities.begin(), bsp->lumpEntities.end() };
	std::stringstream ss( entities );

	std::string line;
	while ( std::getline( ss, line, '\n' ) ) {
		ImGui::Text( line.c_str() );
	}
}

/*
	Name: WindowManager::Window_EntityPartitions
	Purpose: Draws the "Entity Partitions" window
*/
void WindowManager::Window_EntityPartitions() {
	static bool ignoreHeader = false;
	ImGui::Checkbox( "Ignore header", &ignoreHeader );


	std::string patritions = { bsp->lumpEntitiyPartitions.begin(), bsp->lumpEntitiyPartitions.end() };
	std::stringstream ss( patritions );
	
	std::string p;
	std::size_t count = 0;
	while ( std::getline( ss, p, ' ' ) ) {
		if ( count == 0 && ignoreHeader ) {
			count++;
			continue;
		}
		
		ImGui::Text( p.c_str() );
		count++;
	}
}

/*
	Name: WindowManager::Window_TricollManager
	Purpose: Draws the "Tricoll Headers" window
*/
void WindowManager::Window_TricollHeaders() {
	ImGui::Text( "Count: %li", bsp->lumpTitanfallTricollHeaders.size() );


	ImGui::BeginTable( "TricollHeaders", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg );
	ImGui::TableSetupColumn( "Index" );
	ImGui::TableSetupColumn( "Value" );
	ImGui::TableHeadersRow();

	for( std::size_t i = 0; i < bsp->lumpTitanfallTricollHeaders.size(); i++ ) {
		Titanfall::TricollHeader_t header = bsp->lumpTitanfallTricollHeaders.at( i );

		ImGui::TableNextColumn();
			std::string index = std::to_string( i );
			if ( ImGui::TreeNode( index.c_str() ) ){
				ImGui::TreePop();

				ImGui::TableNextColumn();
				
				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    flags" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", header.flags );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    unknown" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", header.unknown );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    material" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", header.material );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    vertexCount" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", header.vertexCount );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    bevelCount" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", header.bevelCount );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    vertexOffset" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", header.vertexOffset );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    bevelOffset" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", header.bevelOffset );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    tricollNode" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", header.tricollNode );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    bevelIndexCount" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", header.bevelIndexCount );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    unk0" );
				ImGui::TableNextColumn();
				ImGui::Text( "%f", header.unk0 );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    unk1" );
				ImGui::TableNextColumn();
				ImGui::Text( "%f", header.unk1 );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    unk2" );
				ImGui::TableNextColumn();
				ImGui::Text( "%f", header.unk2 );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    unk3" );
				ImGui::TableNextColumn();
				ImGui::Text( "%f", header.unk3 );
			}

			ImGui::TableNextRow();
	}

	ImGui::EndTable();
}

/*
	Name: WindowManager::Window_BVHNodes
*/
void WindowManager::Window_BVHNodes() {
	ImGui::Text( "Count: %li", bsp->lumpApexLegendsBVHNodes.size() );
	ImGui::Separator();


	ImGui::BeginTable( "BVHNodes", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg );
	ImGui::TableSetupColumn( "Index" );
	ImGui::TableSetupColumn( "Value" );
	ImGui::TableHeadersRow();

	for( std::size_t i = 0; i < bsp->lumpApexLegendsBVHNodes.size(); i++ ) {
		ApexLegends::BVHNode_t &node = bsp->lumpApexLegendsBVHNodes[i];

		ImGui::TableNextColumn();

		std::string index = std::to_string( i );
		if ( ImGui::TreeNode( index.c_str() ) ){
			ImGui::TreePop();

			ImGui::TableNextRow();

			/*for( int i = 0; i < 32; i++ ) {
				ImGui::TableNextColumn();
				ImGui::TableNextColumn();
				ImGui::Text( "%i", node.unknown[i] );
				ImGui::TableNextRow();
			}*/
		}

		ImGui::TableNextRow();
	}
	
	ImGui::EndTable();
}

/*
	Name: WindowManager::Window_PackedVertices
*/
void WindowManager::Window_PackedVertices() {
	ImGui::Text( "Count: %li", bsp->lumpApexLegendsPackedVertices.size() );
	ImGui::Separator();

	for( Vector3h &vertex : bsp->lumpApexLegendsPackedVertices )
		ImGui::Text( "%i: %i: %i", vertex.x, vertex.y, vertex.z );
}

/*
	Name: WindowManager::Window_Meshes
	Purpose: Draws the "Meshes" window
*/
void WindowManager::Window_Meshes() {
	std::size_t meshes;
	if( GetGameTypeFromVersion( bsp->header.version ) != APEX_LEGENDS )
		meshes = bsp->lumpTitanfallMeshes.size();
	else
		meshes = bsp->lumpApexLegendsMeshes.size();


	ImGui::Text( "Meshes: %li", meshes );
	ImGui::Separator();

	ImGui::BeginTable( "Meshes", 2, ImGuiTableFlags_Resizable | ImGuiTableFlags_Borders | ImGuiTableFlags_RowBg );
	ImGui::TableSetupColumn( "Index" );
	ImGui::TableSetupColumn( "Value" );
	ImGui::TableHeadersRow();

	// Titanfall
	if ( GetGameTypeFromVersion( bsp->header.version ) != APEX_LEGENDS ) {
		for ( std::size_t i = 0; i < bsp->lumpTitanfallMeshes.size(); i++ ) {
			Titanfall::Mesh_t &mesh = bsp->lumpTitanfallMeshes[i];

			ImGui::TableNextColumn();
			std::string index = std::to_string( i );
			if ( ImGui::TreeNode( index.c_str() ) ){
				ImGui::TreePop();

				ImGui::TableNextColumn();
				ImGui::PushID( i );
				if ( ImGui::Button( "Highlight" ) ) {
					viewport->HighlightMesh( i );
				}
				ImGui::PopID();
				
				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    triangleOffset" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.triangleOffset );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    triangleCount" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.triangleCount );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    vertexOffset" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.vertexOffset );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    vertexCount" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.vertexCount );

				for ( int j = 0; j < 6; j++ ) {
					ImGui::TableNextRow();
					ImGui::TableNextColumn();
					ImGui::Text( "    unknown %i", j );
					ImGui::TableNextColumn();
					ImGui::Text( "%i", mesh.unknown[j] );
				}

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    materialOffset" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.materialOffset );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    flags" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.flags );
			}
			else {
				ImGui::TableNextColumn();
				ImGui::PushID( i );
				if ( ImGui::Button( "Highlight" ) ) {
					viewport->HighlightMesh( i );
				}
				ImGui::PopID();
			}

			ImGui::TableNextRow();
		}
	}
	// Apex Legends
	else {
		for ( std::size_t i = 0; i < bsp->lumpApexLegendsMeshes.size(); i++ ) {
			ApexLegends::Mesh_t &mesh = bsp->lumpApexLegendsMeshes[i];

			ImGui::TableNextColumn();
			std::string index = std::to_string( i );
			if ( ImGui::TreeNode( index.c_str() ) ){
				ImGui::TreePop();
				ImGui::TableNextColumn();
				ImGui::PushID( i );
				if ( ImGui::Button( "Highlight" ) ) {
					viewport->HighlightMesh( i );
				}
				ImGui::PopID();
				
				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    triangleOffset" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.triangleOffset );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    triangleCount" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.triangleCount );

				for ( int i = 0; i < 8; i++ ) {
					ImGui::TableNextRow();
					ImGui::TableNextColumn();
					ImGui::Text( "    unknown %i", i );
					ImGui::TableNextColumn();
					ImGui::Text( "%i", mesh.unknown[i] );
				}

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    materialOffset" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.materialOffset );

				ImGui::TableNextRow();
				ImGui::TableNextColumn();
				ImGui::Text( "    flags" );
				ImGui::TableNextColumn();
				ImGui::Text( "%i", mesh.flags );
			}
			else {
				ImGui::TableNextColumn();
				ImGui::PushID( i );
				if ( ImGui::Button( "Highlight" ) ) {
					viewport->HighlightMesh( i );
				}
				ImGui::PopID();
			}

			ImGui::TableNextRow();
		}
	}


	ImGui::EndTable();
}

/*
	Name: WindowManager::TitanfallCellAABBNodesWalker
	Purpose: Walks the Titanfall Cell AABB Node Tree
*/
std::size_t WindowManager::TitanfallCellAABBNodesWalker( uint32_t i, bool draw ) {
	Titanfall::CellAABBNode_t &node = bsp->lumpTitanfallCellAABBNodes[i];

	// The tree can have multiple root nodes
	// We use this to automatically figure out how many there are
	std::size_t children = 1;
	for ( int n = 0; n < node.childCount; n++ ) {
		children += TitanfallCellAABBNodesWalker( node.childOffset + n, false );
	}

	if ( !draw )
		return children;

	std::string index;

	if ( node.childCount )
		index += "Node_";
	else
		index += "Leaf_";

	index += std::to_string( i );
	if ( ImGui::TreeNode( index.c_str() ) ) {
		if ( type == CELL_AABB_NODES ) {
			ImGui::SameLine( ImGui::GetWindowSize()[0] - 80 );
			
			ImGui::PushID( i );
			if ( ImGui::Button( "Highlight" ) ) {
				viewport->HighlightMesh( i );
			}
			ImGui::PopID();
		}

		for ( int n = 0; n < node.childCount; n++ ) {
			TitanfallCellAABBNodesWalker( node.childOffset + n, true );
		}
		ImGui::TreePop();
	}
	else {
		if ( type == CELL_AABB_NODES ) {
			ImGui::SameLine( ImGui::GetWindowSize()[0] - 80 );
			
			ImGui::PushID( i );
			if ( ImGui::Button( "Highlight" ) ) {
				viewport->HighlightMesh( i );
			}
			ImGui::PopID();
		}
	}

	return children;
}

/*
	Name: WindowManager::ApexLegendsCellAABBNodesWalker
	Purpose: Walks the Apex Legends Cell AABB Node Tree
*/
std::size_t WindowManager::ApexLegendsCellAABBNodesWalker( uint32_t i, bool draw ) {
	ApexLegends::CellAABBNode_t &node = bsp->lumpApexLegendsCellAABBNodes[i];

	// The tree can have multiple root nodes
	// We use this to automatically figure out how many there are

	//uint8_t childCount = (uint8_t)node.pain;
	//uint16_t childOffset = (uint16_t)(node.pain >> 8);

	std::size_t children = 1;
	for ( int n = 0; n < node.childCount; n++ ) {
		children += ApexLegendsCellAABBNodesWalker( node.childOffset + n, false );
	}

	if ( !draw )
		return children;

	std::string index;

	if ( node.childFlags )
		index += "Node_";
	else
		index += "Leaf_";

	// std::format s ugly brother
	index += std::to_string( i ) + " - ";
	index += std::to_string( node.objRefCount );
	index += "; " + std::to_string( node.objRefOffset );
	index += "; " + std::to_string( node.objRefFlags );
	if ( ImGui::TreeNode( index.c_str() ) ) {
		if ( type == CELL_AABB_NODES ) {
			ImGui::SameLine( ImGui::GetWindowSize()[0] - 80 );
			
			ImGui::PushID( i );
			if ( ImGui::Button( "Highlight" ) ) {
				viewport->HighlightMesh( i );
			}
			ImGui::PopID();
		}

		for ( int n = 0; n < node.childCount; n++ ) {
			ApexLegendsCellAABBNodesWalker( node.childOffset + n, true );
		}
		ImGui::TreePop();
	}
	else {
		if ( type == CELL_AABB_NODES ) {
			ImGui::SameLine( ImGui::GetWindowSize()[0] - 80 );
			
			ImGui::PushID( i );
			if ( ImGui::Button( "Highlight" ) ) {
				viewport->HighlightMesh( i );
			}
			ImGui::PopID();
		}
	}

	return children;
}

/*
	Name: WindowManager::Window_CellAABBNodes
	Purpose: Draws the "Cell AABB Nodes" window
*/
void WindowManager::Window_CellAABBNodes() {
	if ( GetGameTypeFromVersion( bsp->header.version ) != APEX_LEGENDS )
		ImGui::Text( "Total Nodes: %li", bsp->lumpTitanfallCellAABBNodes.size() );
	else
		ImGui::Text( "Total Nodes: %li", bsp->lumpApexLegendsCellAABBNodes.size() );

	ImGui::Separator();

	//std::size_t funnyCount = 0;

	// Titanfall
	if ( GetGameTypeFromVersion( bsp->header.version ) != APEX_LEGENDS ) {
		std::size_t nodesCovered = 0;
		int roots = 0;


		while ( nodesCovered != bsp->lumpTitanfallCellAABBNodes.size() ) {
			nodesCovered += TitanfallCellAABBNodesWalker( roots, true );
			roots++;
		}

		ImGui::Separator();
		ImGui::Text( "Root Nodes: %i", roots );
	}
	// Apex Legends
	else {
		std::size_t nodesCovered = 0;
		int roots = 0;


		while ( nodesCovered != bsp->lumpApexLegendsCellAABBNodes.size() ) {
			nodesCovered += ApexLegendsCellAABBNodesWalker( roots, true );
			roots++;
		}

		ImGui::Separator();
		ImGui::Text( "Root Nodes: %i", roots );
	}
}

/*
	Name: WindowManager::Window_ObjReferences
	Purpose: Draws the "Obj References" window
*/
void WindowManager::Window_ObjReferences() {
	if ( GetGameTypeFromVersion( bsp->header.version ) != APEX_LEGENDS )
		ImGui::Text( "Total Refs: %li", bsp->lumpTitanfallObjReferences.size() );
	else
		ImGui::Text( "Total Refs: %li", bsp->lumpApexLegendsObjReferences.size() );

	ImGui::Separator();
	
	// Titanfall
	if ( GetGameTypeFromVersion( bsp->header.version ) != APEX_LEGENDS ) {
		for ( uint16_t &ref : bsp->lumpTitanfallObjReferences )
			ImGui::Text( "%i", ref );
	}
	// Apex Legends
	else {
		for ( uint32_t &ref : bsp->lumpApexLegendsObjReferences )
			ImGui::Text( "%i", ref );
	}


}

/*
	Name: WindowManager::Window_CMGrid
	Purpose: Draws the "CM Grid" window
*/
void WindowManager::Window_CMGrid() {
	ImGui::Text( "unknownf: %f", bsp->lumpTitanfallCMGrid[0].unknownf );
	for ( int i = 0; i < 6; i++ ) {
		ImGui::Text( "unknown: %i", bsp->lumpTitanfallCMGrid[0].unknown[i] );
	}
}

/*
	Name: WindowManager::Window_CMGridCells
	Purpose: Draws the "CM Grid Cells" window
*/
void WindowManager::Window_CMGridCells() {
	ImGui::Text( "Count: %li", bsp->lumpTitanfallCMGridCells.size() );
	for ( std::size_t i = 0; i < bsp->lumpTitanfallCMGridCells.size(); i++ ) {
		Titanfall::CMGridCell_t &cell = bsp->lumpTitanfallCMGridCells[i];
		ImGui::Text( "%i; %i", cell.firstGeoSet, cell.geoSetCount );
		if( type == CM_BRUSHES ) {
			ImGui::SameLine( ImGui::GetWindowSize()[0] - 80 );

			ImGui::PushID( i );
			if ( ImGui::Button( "Highlight" ) ) {
				if( cell.geoSetCount )
				{
					std::vector<std::size_t> indices;
					for( std::size_t index = cell.firstGeoSet; index < cell.geoSetCount + cell.firstGeoSet; index++ ) {
						uint16_t shape = bsp->lumpTitanfallCMGeoSets[index].collisionShapeIndex;
						indices.emplace_back( shape );
					}
					viewport->HighlightMeshes( indices );
				}
			}
			ImGui::PopID();
		}
	}
}

/*
	Name: WindowManager::Window_CMGeoSets
	Purpose: Draws the "CM Geo Sets" window
*/
void WindowManager::Window_CMGeoSets() {
	ImGui::Text( "Count: %li", bsp->lumpTitanfallCMGeoSets.size() );
	ImGui::Separator();
	std::size_t index = 0;
	for( Titanfall::CMGeoSet_t &set : bsp->lumpTitanfallCMGeoSets ) {

		ImGui::Text( "Set_%li: %i, %i :: %i, %i, %i", index, set.unknown0, set.unknown1, set.collisionShapeCount, set.collisionShapeIndex, set.collisionShapeType );
		index++;
	}
}

/*
	Name: WindowManager::Window_CMPrimitives
	Purpose: Draws the "CM Primitives" window
*/
void WindowManager::Window_CMPrimitives() {
	ImGui::Text( "Count: %li", bsp->lumpTitanfallCMPrimitives.size() );
	ImGui::Separator();
	
	for( Titanfall::CMPrimitive_t &primitive : bsp->lumpTitanfallCMPrimitives ) {
		ImGui::Text( "%i, %i", primitive.start, primitive.count );
	}
}

/*
	Name: WindowManager::Window_CMBounds
	Purpose: Draws the "CM Geo Set Bounds" or "CM Primitive Bounds" window
*/
void WindowManager::Window_CMBounds( int lump ) {
	std::vector<Titanfall::CMBounds_t> bounds;
	if ( lump == CM_GEO_SET_BOUNDS )
		bounds = bsp->lumpTitanfallCMGeoSetBounds;
	else if ( lump == CM_PRIMITIVE_BOUNDS )
		bounds = bsp->lumpTitanfallCMPrimitiveBounds;
	

	ImGui::Text( "Count: %li", bounds.size() );
	ImGui::Separator();
	for( std::size_t i = 0; i < bounds.size(); i++ ) {
		Titanfall::CMBounds_t &bound = bounds[i];

		ImGui::Text( "Bound_%li: O, %i, %i, E, %i, %i", i, bound.unknown0, bound.unknown1, bound.unknown2, bound.unknown3 );

		if ( type == lump ) {
			ImGui::SameLine( ImGui::GetWindowSize()[0] - 80 );
			
			ImGui::PushID( i );
			if ( ImGui::Button( "Highlight" ) ) {
				viewport->HighlightMesh( i );
			}
			ImGui::PopID();
		}

	}
}

/*
	Name: WindowManager::Window_CMBrushes
	Purpose: Draws the "CMBrushes" widnow
*/
void WindowManager::Window_CMBrushes() {
	ImGui::Text( "Count: %li", bsp->lumpTitanfallCMBrushes.size() );
	ImGui::Separator();

	for ( std::size_t i = 0; i < bsp->lumpTitanfallCMBrushes.size(); i++ ) {
		Titanfall::CMBrush_t &brush = bsp->lumpTitanfallCMBrushes[i];
		ImGui::Text( "Brush_%li: O %i %i E %i", i, brush.unknown, brush.planeCount, brush.planeSideOffset );

		if ( type == CM_BRUSHES ) {
			ImGui::SameLine( ImGui::GetWindowSize()[0] - 80 );
			
			ImGui::PushID( i );
			if ( ImGui::Button( "Highlight" ) ) {
				viewport->HighlightMesh( i );
			}
			ImGui::PopID();
		}
	}
}

/*
	Name: WindowManager::Window_CMBrushSidePlaneOffsets
	Purpose: Draws the "CM Brush Side Plane Offsets" widnow
*/
void WindowManager::Window_CMBrushSidePlaneOffsets() {
	ImGui::Text( "Count: %li", bsp->lumpTitanfallCMBrushSidePlaneOffsets.size() );
	ImGui::Separator();

	for ( std::size_t i = 0; i < bsp->lumpTitanfallCMBrushSidePlaneOffsets.size(); i++ ) {
		ImGui::Text( "Offset_%li: %i", i, bsp->lumpTitanfallCMBrushSidePlaneOffsets[i] );
	}
}

/*
	Name: WindowManager::Window_CMBrushSideProperties
	Purpose: Draws the "CM Brush Side Properties" window
*/
void WindowManager::Window_CMBrushSideProperties() {
	ImGui::Text( "Count: %li", bsp->lumpTitanfallCMBrushSideProperties.size() );
	ImGui::Separator();

	for ( std::size_t i = 0; i < bsp->lumpTitanfallCMBrushSideProperties.size(); i++ ) {
		ImGui::Text( "Prop_%li: %i", i, bsp->lumpTitanfallCMBrushSideProperties[i] );
	}
}

/*
	Name: WindowManager::Window_LevelInfo
	Purpose: Draws the "LevelInfo" window
*/
void WindowManager::Window_LevelInfo() {
	int game = GetGameTypeFromVersion( bsp->header.version );

	// Titanfall
	if ( game != APEX_LEGENDS ) {
		ImGui::Text( "Unknown0: %i", bsp->lumpTitanfallLevelInfo[0].unknown0 );
		ImGui::Text( "Unknown1: %i", bsp->lumpTitanfallLevelInfo[0].unknown1 );
		ImGui::Text( "Unknown2: %i", bsp->lumpTitanfallLevelInfo[0].unknown2 );
		ImGui::Text( "Prop Count: %i", bsp->lumpTitanfallLevelInfo[0].propCount );
		ImGui::Text( "unknownf: ( %f, %f, %f )", bsp->lumpTitanfallLevelInfo[0].unknownf.x, bsp->lumpTitanfallLevelInfo[0].unknownf.y, bsp->lumpTitanfallLevelInfo[0].unknownf.z );

		ImGui::Separator();
		ImGui::Text( "TODO: Put validation logic here" );
		for( int j = 0; j < 32; j++ ) {
			int count = 0;
			for( uint32_t i = 0; i < bsp->lumpTitanfallMeshes.size(); i++ ) {
				Titanfall::Mesh_t &mesh = bsp->lumpTitanfallMeshes[i];
				if( mesh.flags & (1 << j) )
					count++;
			}
			ImGui::Text("Count_0x%x: %i",(1<<j),count);
		}
	}
	// Apex Legends
	else {
		for ( int i = 0; i < 5; i++ ) {
			ImGui::Text( "Unknown %i: %i", i, bsp->lumpApexLegendsLevelInfo[0].unknown[i] );
		}
		ImGui::Text( "unknwonf: ( %f, %f, %f )", bsp->lumpApexLegendsLevelInfo[0].unknownf.x, bsp->lumpApexLegendsLevelInfo[0].unknownf.y, bsp->lumpApexLegendsLevelInfo[0].unknownf.z );
		ImGui::Text( "unknown1: %i", bsp->lumpApexLegendsLevelInfo[0].unknown1 );

		ImGui::Separator();
		ImGui::Text( "TODO: Put validation logic here" );
		for( int j = 0; j < 32; j++ ) {
			int count = 0;
			for( uint32_t i = 0; i < bsp->lumpApexLegendsMeshes.size(); i++ ) {
				ApexLegends::Mesh_t &mesh = bsp->lumpApexLegendsMeshes[i];
				if( mesh.flags & (1 << j) )
					count++;
			}
			ImGui::Text("Count_0x%x: %i",(1<<j),count);
		}
	}

}

/*
	Name: WindowManager::UpdateLumpWindows
	Purpose: Updates all open lump windows
*/
void WindowManager::UpdateLumpWindows() {
	int game = GetGameTypeFromVersion( bsp->header.version );

	for ( int i = 0; i < 128; i++ ) {
		if ( !windows[i] )
			continue;

		Lump_t lump = GetLumpInfo( i, game );
		ImGui::Begin( lump.name.c_str(), &windows[i] );

		if ( ImGui::IsWindowCollapsed() ) {
			ImGui::End();
			return;
		}

		switch( i ) {
			case ENTITIES:
				Window_Entities();
				break;
			case MODELS:
				Window_Models();
				break;
			case ENTITY_PARTITIONS:
				Window_EntityPartitions();
				break;
			case TEXTURE_DATA_STRING_DATA:
			case SURFACE_NAMES:
				Window_SurfaceNames();
				break;
			case TRICOLL_HEADERS:
				Window_TricollHeaders();
				break;
			case BVH_NODES:
				Window_BVHNodes();
				break;
			case PACKED_VERTICES:
				Window_PackedVertices();
				break;
			case MESHES:
				Window_Meshes();
				break;
			case CM_GRID:
				Window_CMGrid();
				break;
			case CM_GRID_CELLS:
				Window_CMGridCells();
				break;
			case CM_GEO_SETS:
				Window_CMGeoSets();
				break;
			case CM_GEO_SET_BOUNDS:
				Window_CMBounds( CM_GEO_SET_BOUNDS );
				break;
			case CM_PRIMITIVES:
				Window_CMPrimitives();
				break;
			case CM_PRIMITIVE_BOUNDS:
				Window_CMBounds( CM_PRIMITIVE_BOUNDS );
				break;
			case CM_BRUSHES:
				Window_CMBrushes();
				break;
			case CM_BRUSH_SIDE_PLANE_OFFSETS:
				Window_CMBrushSidePlaneOffsets();
				break;
			case CM_BRUSH_SIDE_PROPERTIES:
				Window_CMBrushSideProperties();
				break;
			case CELL_AABB_NODES:
				Window_CellAABBNodes();
				break;
			case OBJ_REFERENCES:
				Window_ObjReferences();
				break;
			case LEVEL_INFO:
				Window_LevelInfo();
				break;
		};

		ImGui::End();
	}
}