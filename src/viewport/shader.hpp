// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#pragma once

#include <fstream>
#include <string>
// OpenGL
#include <GL/glew.h>
#include <GL/gl.h>

/*
	Class: Shader
	Purpose: Compiles both the fragment and vertex shader and stores their program
*/
class Shader {
	public:
	unsigned int id;

	/*
		Name: Shader
		Purpose: Class constructor
	*/
	Shader( std::string vertPath, std::string fragPath ) {
		
		unsigned int vertex, fragment;
		int success;
		char log[512];

		// Compile vertex shader
		std::ifstream vertFile( vertPath );
		std::string vertString( ( std::istreambuf_iterator<char>( vertFile ) ),
							( std::istreambuf_iterator<char>() ) );

		const char* vertCode = vertString.c_str();
		vertex = glCreateShader( GL_VERTEX_SHADER );
		glShaderSource( vertex, 1, &vertCode, NULL );
		glCompileShader( vertex );
		glGetShaderiv( vertex, GL_COMPILE_STATUS, &success );
		if( !success ) {
			glGetShaderInfoLog( vertex, 512, NULL, log );
			printf( "Vertex shader compilation error!\n%s\n", log );
		}

		// Compile fragment shader
		std::ifstream fragFile( fragPath );
		std::string fragString( ( std::istreambuf_iterator<char>( fragFile ) ),
							( std::istreambuf_iterator<char>() ) );

		const char* fragCode = fragString.c_str();
		fragment = glCreateShader( GL_FRAGMENT_SHADER );
		glShaderSource( fragment, 1, &fragCode, NULL );
		glCompileShader( fragment );
		glGetShaderiv( fragment, GL_COMPILE_STATUS, &success );
		if( !success ) {
			glGetShaderInfoLog( fragment, 512, NULL, log );
			printf( "Fragment shader compilation error!\n%s\n", log );
		}


		// Compile program
		id = glCreateProgram();
		glAttachShader( id, vertex );
		glAttachShader( id, fragment );
		glLinkProgram( id );
		glGetProgramiv( id, GL_LINK_STATUS, &success );
		if( !success ) {
			glGetProgramInfoLog( id, 512, NULL, log );
			printf( "Program compilation error!\n%s\n", log );
		}

		glDeleteShader( vertex );
		glDeleteShader( fragment );
	}
};