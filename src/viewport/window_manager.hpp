// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#pragma once

#include <sstream>
#include "imgui.h"
#include "backends/imgui_impl_opengl3.h"
#include "backends/imgui_impl_glfw.h"
#include "../bsp.hpp"
#include "../games/games.hpp"
#include "viewport.hpp"
// OpenGL
#include <GL/glew.h>
#include <GL/gl.h>
// GLFW
#include <GLFW/glfw3.h>

/*
	Class: WindowManager
	Purpose: Handles all of ImGui
*/
class WindowManager {
	// Private
	private:
	Bsp *bsp;
	Viewport *viewport;
	bool windows[128];
	int polygonMode = POLY_NONE;
	int type = MESHES;

	void Window_BspInfo();
	void Window_ViewportInfo();

	void Window_Models();
	void Window_SurfaceNames(); // Used for both tf and al even though the lumps have different indexes
	void Window_Entities();
	void Window_EntityPartitions();
	void Window_TricollHeaders();
	void Window_BVHNodes();
	void Window_PackedVertices();
	void Window_Meshes();
	void Window_CMGrid();
	void Window_CMGridCells();
	void Window_CMGeoSets();
	void Window_CMPrimitives();
	void Window_CMBounds( int lump );
	void Window_CMBrushes();
	void Window_CMBrushSidePlaneOffsets();
	void Window_CMBrushSideProperties();
	void Window_LevelInfo();
	std::size_t TitanfallCellAABBNodesWalker( uint32_t i, bool draw );
	std::size_t ApexLegendsCellAABBNodesWalker( uint32_t i, bool draw );
	void Window_CellAABBNodes();
	void Window_ObjReferences();
	void UpdateLumpWindows();

	// Public
	public:
	WindowManager( GLFWwindow *window, Viewport &view, Bsp &map );
	void Update();
};