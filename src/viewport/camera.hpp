// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

enum eCameraMoveDirection {
	MOVE_FORWARD,
	MOVE_BACKWARD,
	MOVE_LEFT,
	MOVE_RIGHT,
	MOVE_UP,
	MOVE_DOWN
};



/*
	Class: Camera
	Purpose: Hold the view matrix used to move around the scene
*/
class Camera {
	private:
		float fov = 70;

		double xPosLast, yPosLast;

		glm::vec3 position = glm::vec3( 0.0f, 0.0f, 0.0f );
		float yaw = 0.0f;
		float pitch = -90.0f;

		glm::vec3 front, right;
	public:
		glm::mat4 viewMatrix = glm::mat4( 1.0f );

		/*
			Name: Update
			Purpose: Calculates new position & rotation
		*/
		void Update( float width, float height ) {
			// Calculate direction vectors
			front.x = sin( glm::radians( yaw ) ) * cos(glm::radians(pitch + 90));
			front.y = cos( glm::radians( yaw ) ) * cos(glm::radians(pitch + 90));
			front.z = -sin( glm::radians( pitch + 90 ) );

			right = glm::normalize( glm::cross( glm::vec3( 0.0, 0.0, -1.0f ), front ) );
			front = glm::normalize( front );


			glm::mat4 perspective = glm::perspective( glm::radians( fov ), width / height, 0.1f, 1000000.0f );


			viewMatrix = glm::rotate( perspective, glm::radians( pitch ), glm::vec3( 1.0, 0.0, 0.0 ) );
			viewMatrix = glm::rotate( viewMatrix, glm::radians( yaw ), glm::vec3( 0.0, 0.0, 1.0 ) );

			viewMatrix = glm::translate( viewMatrix, position );
		}

		/*
			Name: Move
			Purpose: Moves the camera in the desired direction
		*/
		void Move( int type, float speed ) {
			switch ( type ) {
				case MOVE_FORWARD:
					position -= front * speed;
					return;
				case MOVE_BACKWARD:
					position += front * speed;
					return;
				case MOVE_LEFT:
					position += right * speed;
					return;
				case MOVE_RIGHT:
					position -= right * speed;
					return;
				case MOVE_UP:
					position.z -= speed;
					return;
				case MOVE_DOWN:
					position.z += speed;
					return;
			};
		}

		/*
			Name: Rotate
			Purpose: Rotates the camera
		*/
		void Rotate ( double xAxis, double yAxis, bool ignore ) {

			if ( ignore ) {
				xPosLast = xAxis; yPosLast = yAxis;
				return;
			}

			double xMovement, yMovement;

			xMovement = xPosLast - xAxis;
			yMovement = yPosLast - yAxis;

			pitch -= yMovement * 0.2;
			yaw -= xMovement * 0.2;

			pitch = pitch > 0 ? 0: pitch;
			pitch = pitch < -180 ? -180: pitch;

			xPosLast = xAxis; yPosLast = yAxis;
		}
};
