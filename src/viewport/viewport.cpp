// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#include "viewport.hpp"

/*
	Name: Viewport::Viewport
	Purpose: Class constructor
*/
Viewport::Viewport( std::string path ) {
	glEnable( GL_DEPTH_TEST );
	glCullFace( GL_FRONT );

	// We dont know which folder we're being launched from so we need to compensate like this
	std::string exeFolder = { path.begin(), path.begin() + path.rfind( SLASH ) + 1 };

	basicShader = new Shader( exeFolder + "../shaders/basic.vert", exeFolder + "../shaders/basic.frag" );
}

/*
	Name: Viewport::~Viewport
	Purpose: Class deconstructor
*/
Viewport::~Viewport() {
	delete basicShader;
}

/*
	Name: Viewport::Update
	Purpose: Draws the frame
*/
void Viewport::Update( int width, int height ) {
	// Update Camera
	camera.Update( width, height );

    unsigned int transLoc = glGetUniformLocation( basicShader->id, "transform" );
    glUniformMatrix4fv( transLoc, 1, GL_FALSE, glm::value_ptr( camera.viewMatrix ) );

	// Colors we use to shade unselected meshes
	glm::fvec3 colors[4] = {
		glm::fvec3(0.5f, 0.9f, 0.9f),
		glm::fvec3(0.6f, 0.9f, 0.5f),
		glm::fvec3(0.8f, 0.5f, 0.9f),
		glm::fvec3(0.9f, 0.6f, 0.5f)
	};

	// Loop through meshes
	for( std::size_t i = 0; i < drawable.meshes.size(); i++ ) {
		Mesh_t &mesh = drawable.meshes[i];

		glm::fvec3 color = colors[i % 4];
		unsigned int colorLoc = glGetUniformLocation( basicShader->id, "base" );
		glUniform3fv( colorLoc, 1, glm::value_ptr( color ) );

		if ( *polygonMode == POLY_NONE ) {
			if ( mesh.wireframe )
				glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
			else
				glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
		}

		glDrawElements( GL_TRIANGLES, mesh.end - mesh.start + 1, GL_UNSIGNED_INT, (void*)(sizeof(GLuint) * mesh.start ) );
	}

}

/*
	Name: Viewport::SetDrawable
	Purpose: Sets the active drawable
*/
void Viewport::SetDrawable( Drawable newDrawable ) {
	drawable = newDrawable;

	glGenBuffers( 1, &vertexBuffer );
	glBindBuffer( GL_ARRAY_BUFFER, vertexBuffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof( DrawableVertex_t ) * drawable.vertexCount, drawable.vertices, GL_STATIC_DRAW );

	glGenBuffers( 1, &indexBuffer );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indexBuffer );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(uint32_t) * drawable.indexCount, drawable.indices, GL_STATIC_DRAW );

	glUseProgram( basicShader->id );


	glEnableVertexAttribArray( 0 );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, sizeof( DrawableVertex_t ), (void*)offsetof( DrawableVertex_t, position ) );
	glEnableVertexAttribArray( 1 );
	glVertexAttribPointer( 1, 3, GL_FLOAT, GL_FALSE, sizeof( DrawableVertex_t ), (void*)offsetof( DrawableVertex_t, normal ) );

}

/*
	Name: Viewport::HighlightMesh
	Purpose: Force a mesh to draw / not draw in wireframe mode
*/
void Viewport::HighlightMesh( std::size_t index ) {
	for ( std::size_t i = 0; i < drawable.meshes.size(); i++ ) {
		if ( i == index )
			drawable.meshes[ i ].wireframe = false;
		else
			drawable.meshes[ i ].wireframe = true;
	}
}

/*
	Name: Viewport::HighlightMeshesInRange
	Purpose: Force a mesh to draw / not draw in wireframe mode
*/
void Viewport::HighlightMeshesInRange( std::size_t first, std::size_t last ) {
	for ( std::size_t i = 0; i < drawable.meshes.size(); i++ ) {
		if ( i >= first && i <= last )
			drawable.meshes[ i ].wireframe = false;
		else
			drawable.meshes[ i ].wireframe = true;
	}
}

/*
	Name: Viewport::HighlightMeshes
	Purpose: Force a mesh to draw / not draw in wireframe mode
*/
void Viewport::HighlightMeshes( std::vector<std::size_t> indices ) {
	for ( std::size_t i = 0; i < drawable.meshes.size(); i++ ) {
		drawable.meshes[ i ].wireframe = true;
		for( std::size_t mesh : indices ) {
			if ( i == mesh )
				drawable.meshes[ i ].wireframe = false;
		}
	}
}

/*
	Name: Viewport::ResetHighlight
	Purpose: Sets Mesh_t.wireframe to false
*/
void Viewport::ResetHighlight() {
	for ( Mesh_t &mesh : drawable.meshes )
		mesh.wireframe = false;
}