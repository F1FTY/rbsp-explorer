// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#pragma once

#include "../utils.hpp"
#include <vector>
#include <cstdint>

struct Mesh_t {
	uint32_t start;
	uint32_t end;
	bool wireframe = false;
};

struct DrawableVertex_t {
	Vector3f position;
	Vector3f normal;
};


/*
	Class: Drawable
	Purpose: Holds vertex & triangle data
*/
class Drawable {
	public:
	~Drawable() {
		// Calling this causes a Segmentation fault when program is closed
		// Since these were created with `new` I'd expect I have to call this ?
		// TODO: Figure this out
		//delete[] vertices;
		//delete[] indices;
	}
	
	DrawableVertex_t *vertices;
	uint32_t vertexCount;
	uint32_t *indices;
	uint32_t indexCount;
	std::vector<Mesh_t> meshes;


	/*
		Name: GetDrawableVerticesFromAABB
		Purpose: Returns a vector of DrawableVertex_t
	*/
	std::vector<DrawableVertex_t> GetDrawableVerticesFromAABB ( AABB &aabb ) {
		std::vector<DrawableVertex_t> vertices;
		DrawableVertex_t vertex;

		vertex.position = Vector3f ( aabb.mins.x, aabb.mins.y, aabb.mins.z );
		vertex.normal = Vector3f ( -0.5, -0.5, -0.5 );
		vertices.push_back( vertex );

		vertex.position = Vector3f ( aabb.mins.x, aabb.maxs.y, aabb.mins.z );
		vertex.normal = Vector3f ( -0.5, 0.5, -0.5 );
		vertices.push_back( vertex );

		vertex.position = Vector3f ( aabb.maxs.x, aabb.maxs.y, aabb.mins.z );
		vertex.normal = Vector3f ( 0.5, 0.5, -0.5 );
		vertices.push_back( vertex );

		vertex.position = Vector3f ( aabb.maxs.x, aabb.mins.y, aabb.mins.z );
		vertex.normal = Vector3f ( 0.5, -0.5, -0.5 );
		vertices.push_back( vertex );

		vertex.position = Vector3f ( aabb.mins.x, aabb.mins.y, aabb.maxs.z );
		vertex.normal = Vector3f ( -0.5, -0.5, 0.5 );
		vertices.push_back( vertex );

		vertex.position = Vector3f ( aabb.mins.x, aabb.maxs.y, aabb.maxs.z );
		vertex.normal = Vector3f ( -0.5, 0.5, 0.5 );
		vertices.push_back( vertex );

		vertex.position = Vector3f ( aabb.maxs.x, aabb.maxs.y, aabb.maxs.z );
		vertex.normal = Vector3f ( 0.5, 0.5, 0.5 );
		vertices.push_back( vertex );

		vertex.position = Vector3f ( aabb.maxs.x, aabb.mins.y, aabb.maxs.z );
		vertex.normal = Vector3f ( 0.5, -0.5, 0.5 );
		vertices.push_back( vertex );

		return vertices;
	}

	/*
		Name: GetIndicesFromAABB
		Purpose: Companion to `GetDrawableVerticesFromAABB`, returns triangle indices
	*/
	std::vector<uint32_t> GetIndicesFromAABB ( AABB &aabb ) {
		std::vector<uint32_t> indices;

		indices.push_back( 3 );
		indices.push_back( 1 );
		indices.push_back( 0 );

		indices.push_back( 3 );
		indices.push_back( 2 );
		indices.push_back( 1 );

		indices.push_back( 4 );
		indices.push_back( 5 );
		indices.push_back( 7 );

		indices.push_back( 5 );
		indices.push_back( 6 );
		indices.push_back( 7 );

		indices.push_back( 0 );
		indices.push_back( 1 );
		indices.push_back( 4 );

		indices.push_back( 1 );
		indices.push_back( 5 );
		indices.push_back( 4 );

		indices.push_back( 1 );
		indices.push_back( 2 );
		indices.push_back( 5 );

		indices.push_back( 2 );
		indices.push_back( 6 );
		indices.push_back( 5 );

		indices.push_back( 2 );
		indices.push_back( 3 );
		indices.push_back( 6 );

		indices.push_back( 3 );
		indices.push_back( 7 );
		indices.push_back( 6 );

		indices.push_back( 3 );
		indices.push_back( 0 );
		indices.push_back( 7 );

		indices.push_back( 0 );
		indices.push_back( 4 );
		indices.push_back( 7 );

		return indices;
	}
};