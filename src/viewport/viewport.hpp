// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#pragma once

#include "../bsp.hpp"
#include "shader.hpp"
#include "drawable.hpp"
#include "camera.hpp"
#include <string>
#include <glm/ext.hpp>


#ifdef _WIN32
#define SLASH '\\'
#else
#define SLASH '/'
#endif

/*
	Class: Viewport
	Purpose: Handles OpenGL, drawing and movement
*/
class Viewport {
	private:
	Drawable drawable;
	Shader *basicShader;

	unsigned int vertexBuffer;
	unsigned int indexBuffer;

	public:
	Camera camera;
	int *polygonMode;

	Viewport() {};
	Viewport( std::string path );
	~Viewport();
	void Update( int width, int height );
	void SetDrawable( Drawable newDrawable );
	void HighlightMesh( std::size_t index );
	void HighlightMeshesInRange( std::size_t first, std::size_t last );
	void HighlightMeshes( std::vector<std::size_t> indices );
	void ResetHighlight();
};