// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#include <stdio.h>
#include "bsp.hpp"
#include "viewport/window_manager.hpp"
#include "viewport/viewport.hpp"
// OpenGL
#include <GL/glew.h>
#include <GL/gl.h>
// GLFW
#include <GLFW/glfw3.h>



bool cursorFocused = false;
bool ignoreFirst = true;
double xPos, yPos;


/*
	Name: PrintHelp()
	Purpose: Outputs instructions on how to use this program
*/
void PrintHelp() {
	printf( "To start exploring run: \"explorer <bsp-path>\"\n" );
}


/*
	Name: Callback_CursorPosition
	Purpose: Saves mouse delta into xPos & yPos
*/
static void Callback_CursorPosition( GLFWwindow* window, double x, double y ) {
	xPos = x;
	yPos = y;
}


/*
	Name: main
*/
int main( int argc, char* argv[] ) {
	printf( "rBSP Explorer\n" );
	printf( "Gitlab: https://gitlab.com/F1FTY/rbsp-explorer\n\n" );

	// A couple checks before we start GLFW
	if ( argc < 2 ) {
		PrintHelp();
		return 0;
	}

	Bsp bsp( argv[1] );

	if ( !bsp.valid ) {
		return -1;
	}

	// GLFW Init
	if( !glfwInit() ) {
		printf( "Failed to initilaze GLFW!\n" );
		return -1;
	}

	GLFWwindow* window;
	window = glfwCreateWindow( 1080, 640, "explorer", NULL, NULL );

	if( !window ) {
		printf( "Failed to create window!\n" );
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent( window );
	glfwSwapInterval( 1 );


	glfwSetCursorPosCallback( window, Callback_CursorPosition );

	if ( glfwRawMouseMotionSupported() )
		glfwSetInputMode( window, GLFW_RAW_MOUSE_MOTION, GLFW_TRUE );


	if( glewInit() != GLEW_OK ) {
		printf( "Failed to initilaze glew!\n" );
		glfwTerminate();
		return -1;
	}

	Viewport viewport( argv[0] );

	WindowManager wm( window, viewport, bsp );


	// Main loop
	float lastFrame = 0.0;
	const float frameLength = 1 / 30; // 30 FPS

	while( !glfwWindowShouldClose( window ) ) {
		glfwPollEvents();

		// Check pressed keys
		// Capture / Free mouse Cursor
		static int lastState_Esc = GLFW_RELEASE;
		int currentState_Esc = glfwGetKey( window, GLFW_KEY_ESCAPE );
		if ( lastState_Esc != currentState_Esc && lastState_Esc == GLFW_RELEASE ) {
			if ( cursorFocused ) {
				glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_NORMAL );
				cursorFocused = !cursorFocused;
			}
			else {
				glfwSetInputMode( window, GLFW_CURSOR, GLFW_CURSOR_DISABLED );
				cursorFocused = !cursorFocused;
				ignoreFirst = true;
			}
		}
		lastState_Esc = currentState_Esc;

		if ( cursorFocused ) {
			float speed = 10.0f;
			if ( glfwGetKey( window, GLFW_KEY_LEFT_SHIFT ) == GLFW_PRESS )
				speed = 100.0f;


			// Move camera
			if ( glfwGetKey( window, GLFW_KEY_W ) == GLFW_PRESS )
				viewport.camera.Move( MOVE_FORWARD, speed );
			if ( glfwGetKey( window, GLFW_KEY_S ) == GLFW_PRESS )
				viewport.camera.Move( MOVE_BACKWARD, speed );
			if ( glfwGetKey( window, GLFW_KEY_A ) == GLFW_PRESS )
				viewport.camera.Move( MOVE_LEFT, speed );
			if ( glfwGetKey( window, GLFW_KEY_D ) == GLFW_PRESS )
				viewport.camera.Move( MOVE_RIGHT, speed );
			if ( glfwGetKey( window, GLFW_KEY_E ) == GLFW_PRESS )
				viewport.camera.Move( MOVE_UP, speed );
			if ( glfwGetKey( window, GLFW_KEY_Q ) == GLFW_PRESS )
				viewport.camera.Move( MOVE_DOWN, speed );

			// Rotate camera
			viewport.camera.Rotate( xPos, yPos, ignoreFirst );
			ignoreFirst = false;
		}


		// Draw
		int display_w, display_h;
		glfwGetFramebufferSize( window, &display_w, &display_h );
		glViewport( 0, 0, display_w, display_h );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

		viewport.Update( display_w, display_h );

		wm.Update();

		glfwSwapBuffers( window );

		lastFrame = glfwGetTime();
		while( glfwGetTime() < lastFrame + frameLength );
	}

	glfwTerminate();
	return 0;
}