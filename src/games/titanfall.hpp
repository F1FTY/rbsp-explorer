// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#pragma once

#include "../utils.hpp"

enum eTitanfallLumps {
	ENTITIES                        = 0,
	PLANES                          = 1,
	TEXTURE_DATA                    = 2,
	VERTICES                        = 3,
	MODELS                          = 14,
	ENTITY_PARTITIONS               = 24,
	PHYSICS_COLLIDE                 = 29,
	VERTEX_NORMALS                  = 30,
	GAME_LUMP                       = 35,
	LEAF_WATER_DATA                 = 36,
	PAKFILE                         = 40,
	CUBEMAPS                        = 42,
	TEXTURE_DATA_STRING_DATA        = 43,
	TEXTURE_DATA_STRING_TABLE       = 44,
	WORLD_LIGHTS                    = 54,
	PHYSICS_LEVEL                   = 62,
	TRICOLL_TRIANGLES               = 66,
	TRICOLL_NODES                   = 68,
	TRICOLL_HEADERS                 = 69,
	PHYSICS_TRIANGLES               = 70,
	VERTEX_UNLIT                    = 71,
	VERTEX_LIT_FLAT                 = 72,
	VERTEX_LIT_BUMP                 = 73,
	VERTEX_UNLIT_TS                 = 74,
	VERTEX_BLINN_PHONG              = 75,
	VERTEX_RESERVED_5               = 76,
	VERTEX_RESERVED_6               = 77,
	VERTEX_RESERVED_7               = 78,
	MESH_INDICES                    = 79,
	MESHES                          = 80,
	MESH_BOUNDS                     = 81,
	MATERIAL_SORT                   = 82,
	LIGHTMAP_HEADERS                = 83,
	CM_GRID                         = 85,
	CM_GRID_CELLS                   = 86,
	CM_GEO_SETS                     = 87,
	CM_GEO_SET_BOUNDS               = 88,
	CM_PRIMITIVES                   = 89,
	CM_PRIMITIVE_BOUNDS             = 90,
	CM_UNIQUE_CONTENTS              = 91,
	CM_BRUSHES                      = 92,
	CM_BRUSH_SIDE_PLANE_OFFSETS     = 93,
	CM_BRUSH_SIDE_PROPERTIES        = 94,
	CM_BRUSH_SIDE_TEXTURE_VECTORS   = 95,
	TRICOLL_BEVEL_STARTS            = 96,
	TRICOLL_BEVEL_INDICES           = 97,
	LIGHTMAP_DATA_SKY               = 98,
	CSM_AABB_NODES                  = 99,
	CSM_OBJ_REFERENCES              = 100,
	LIGHTPROBES                     = 101,
	STATIC_PROP_LIGHTPROBE_INDICES  = 102,
	LIGHTPROBE_TREE                 = 103,
	LIGHTPROBE_REFERENCES           = 104,
	LIGHTMAP_DATA_REAL_TIME_LIGHTS  = 105,
	CELL_BSP_NODES                  = 106,
	CELLS                           = 107,
	PORTALS                         = 108,
	PORTAL_VERTICES                 = 109,
	PORTAL_EDGES                    = 110,
	PORTAL_VERTEX_EDGES             = 111,
	PORTAL_VERTEX_REFERENCES        = 112,
	PORTAL_EDGE_REFERENCES          = 113,
	PORTAL_EDGE_INTERSECT_AT_EDGE   = 114,
	PORTAL_EDGE_INTERSECT_AT_VERTEX = 115,
	PORTAL_EDGE_INTERSECT_HEADER    = 116,
	OCCLUSION_MESH_VERTICES         = 117,
	OCCLUSION_MESH_INDICES          = 118,
	CELL_AABB_NODES                 = 119,
	OBJ_REFERENCES                  = 120,
	OBJ_REFERENCE_BOUNDS            = 121,
	LEVEL_INFO                      = 123,
	SHADOW_MESH_OPAQUE_VERTICES     = 124,
	SHADOW_MESH_ALPHA_VERTICES      = 125,
	SHADOW_MESH_INDICES             = 126,
	SHADOW_MESHES                   = 127
};


static const Lump_t titanfallLumps[128] = {
	{ "Entities", true, 0 },
	{ "Planes", false, 0 },
	{ "Texture Data", false, 0 },
	{ "Vertices", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Models", true, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Entity Partitions", true, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Physics Collide", false, 0 },
	{ "Vertex Normals", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Game Lump", false, 0 },
	{ "Leaf Water Data", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "PakFile", false, 0 },
	{ "Unused", false, 0 },
	{ "CubeMaps", false, 0 },
	{ "Texture Data String Data", true, 0 },
	{ "Texture Data String Table", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "World Lights", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Physics level", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Tricoll Triangles", false, 0 },
	{ "Unused", false, 0 },
	{ "Tricol Nodes", false, 0 },
	{ "Tricoll Headers", true, 0 },
	{ "Physics Triangles", false, 0 },
	{ "Vertex Unlit", false, 0 },
	{ "Vertex Lit Flat", false, 0 },
	{ "Vertex Lit Bump", false, 0 },
	{ "Vertex Unlit Ts", false, 0 },
	{ "Vertex Blinn Phong", false, 0 },
	{ "Vertex Reserved 5", false, 0 },
	{ "Vertex Reserved 6", false, 0 },
	{ "Vertex Reserved 7", false, 0 },
	{ "Mesh Indices", false, 0 },
	{ "Meshes", true, 0 },
	{ "Mesh Bounds", false, 0 },
	{ "Material Sort", false, 0 },
	{ "Lightmap Headers", false, 0 },
	{ "Unused", false, 0 },
	{ "CM Grid", true, 0 },
	{ "CM Grid Cells", true, 0 },
	{ "CM Geo Sets", true, 0 },
	{ "CM Geo Set Bounds", true, 0 },
	{ "CM Primitives", true, 0 },
	{ "CM Primitive Bounds", true, 0 },
	{ "CM Unique Contents", false, 0 },
	{ "CM Brushes", true, 0 },
	{ "CM Brush Side Plane Offsets", true, 0 },
	{ "CM Brush Side Properties", true, 0 },
	{ "CM Brush Side Texture Vectors", false, 0 },
	{ "Tricoll Bevel Starts", false, 0 },
	{ "Tricoll Bevel Indices", false, 0 },
	{ "Lightmap Data Sky", false, 0 },
	{ "CSM AABB Nodes", false, 0 },
	{ "CSM Obj References", false, 0 },
	{ "Lightprobes", false, 0 },
	{ "Static Prop Lightprobe Indices", false, 0 },
	{ "Lightprobe Tree", false, 0 },
	{ "Lightprobe References", false, 0 },
	{ "Lightmap Data Real Time Lights", false, 0 },
	{ "Cell BSP Nodes", false, 0 },
	{ "Cells", false, 0 },
	{ "Portals", false, 0 },
	{ "Portal Vertices", false, 0 },
	{ "Portal Edges", false, 0 },
	{ "Portal Vertex Edges", false, 0 },
	{ "Portal Vertex References", false, 0 },
	{ "Portal Edge References", false, 0 },
	{ "Portal Edge Intersect At Edge", false, 0 },
	{ "Portal Edge Intersect At Vertex", false, 0 },
	{ "Portal Edge Intersect Header", false, 0 },
	{ "Occlusion Mesh Vertices", false, 0 },
	{ "Occlusion Mesh Indices", false, 0 },
	{ "Cell AABB Nodes", true, 0 },
	{ "Obj References", true, 0 },
	{ "Obj Reference Bounds", false, 0 },
	{ "Unused", false, 0 },
	{ "Level Info", true, 0 },
	{ "Shadow Mesh Opaque Vertices", false, 0 },
	{ "Shadow Mesh Alpha Vertices", false, 0 },
	{ "Shadow Mesh Indices", false, 0 },
	{ "Shadow Meshes", false, 0 }
};

namespace Titanfall {
	struct Model_t {
		AABB aabb;
		uint32_t firstMesh;
		uint32_t meshCount;
	};

	struct VertexLitBump_t {
		uint32_t vertexIndex;
		uint32_t normalIndex;
		float uv0[2];
		uint32_t negativeOne;
		float uv1[2];
		float unknown[4];
	};

	struct Mesh_t {
		int32_t triangleOffset;
		uint16_t triangleCount;
		uint16_t vertexOffset;
		uint16_t vertexCount;
		uint16_t unknown[6];
		uint16_t materialOffset;
		uint32_t flags;
	};

	struct MaterialSort_t {
		int16_t textureData;
		int16_t lightmapHeader;
		int16_t cubemap;
		int16_t unknown;
		int32_t vertexOffset;
	};

	struct TricollHeader_t {
		uint16_t flags;
		uint16_t unknown;
		uint16_t material;
		uint16_t vertexCount;
		uint32_t bevelCount;
		uint32_t vertexOffset;
		uint32_t bevelOffset;
		uint32_t tricollNode;
		uint32_t bevelIndexCount;
		float unk0;
		float unk1;
		float unk2;
		float unk3;
	};

	struct CMGrid_t {
		float unknownf;
		uint32_t unknown[6];
	};

	struct CMGridCell_t {
		uint16_t firstGeoSet;
		uint16_t geoSetCount;
	};

	struct CMGeoSet_t {
		uint16_t unknown0;
		uint16_t unknown1;
		uint32_t collisionShapeCount: 8;
		uint32_t collisionShapeIndex: 16;
		uint32_t collisionShapeType: 8;
	};

	struct CMPrimitive_t {
		uint16_t start;
		uint16_t count;
	};

	// Geo Set Bounds & Primitive Bounds are loaded by the same func in-game
	struct CMBounds_t {
		Vector3h origin;
		uint8_t unknown0;
		uint8_t unknown1;
		Vector3h extents;
		uint8_t unknown2;
		uint8_t unknown3;
	};

	struct CMBrush_t {
		Vector3f origin;
		uint8_t unknown;
		uint8_t planeCount;
		uint16_t index;
		Vector3f extents;
		uint32_t planeSideOffset;
	};

	struct CellAABBNode_t {
		Vector3f mins;
		uint8_t childCount;
		uint8_t objRefCount;
		uint16_t totalObjRefCount;
		Vector3f maxs;
		uint16_t childOffset;
		uint16_t objRefOffset;
	};

	struct ObjReferenceBounds_t {
		Vector3f mins;
		uint32_t zero0;
		Vector3f maxs;
		uint32_t zero1;
	};

	struct LevelInfo_t {
		uint32_t unknown0; // 0x200 + 0x8 + 0x4 - 0x10
		uint32_t unknown1; // Mesh.count - 0x400
		uint32_t unknown2; // 0x200 + 0x8 + 0x4
		uint32_t propCount;
		Vector3f unknownf;
	};
}
