// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#pragma once

#include <cstdint>
#include <string>
#include "titanfall.hpp"
#include "titanfall2.hpp"
#include "apex_legends.hpp"


enum eGame {
	TITANFALL,
	TITANFALL2,
	APEX_LEGENDS,
	UNSUPPORTED
};

int GetGameTypeFromVersion( uint32_t version );
std::string GetGameNameFromVersion( int version );

Lump_t GetLumpInfo( int index, int game );

bool IsLumpUsedInGame( int index, int game );

bool IsLumpUsedInTitanfall( int index );
bool IsLumpUsedInTitanfall2( int index );
bool IsLumpUsedInApexLegends( int index );