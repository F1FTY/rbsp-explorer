// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#pragma once

#include "../utils.hpp"

enum eApexLegends {
	//ENTITIES                        = 0,
	//PLANES                          = 1,
	//TEXTURE_DATA                    = 2,
	//VERTICES                        = 3,
	//LIGHTPROBE_PARENT_INFOS         = 4,
	//SHADOW_ENVIRONMENTS             = 5,
	//MODELS                          = 14,
	SURFACE_NAMES                   = 15,
	CONTENTS_MASKS                  = 16,
	SURFACE_PROPERTIES              = 17,
	BVH_NODES                       = 18,
	BVH_LEAF_DATA                   = 19,
	PACKED_VERTICES                 = 20,
	//ENTITY_PARTITIONS               = 24,
	//VERTEX_NORMALS                  = 30,
	//GAME_LUMP                       = 35,
	UNKNOWN_37                      = 37,
	UNKNOWN_38                      = 38,
	UNKNOWN_39                      = 39,
	//PAKFILE                         = 40,
	//CUBEMAPS                        = 42,
	UNKNOWN_43                      = 43,
	//WORLD_LIGHTS                    = 54,
	//WORLD_LIGHT_PARENT_INFOS        = 55,
	//VERTEX_UNLIT                    = 71,
	//VERTEX_LIT_FLAT                 = 72,
	//VERTEX_LIT_BUMP                 = 73,
	//VERTEX_UNLIT_TS                 = 74,
	//VERTEX_BLINN_PHONG              = 75,
	//VERTEX_RESERVED_5               = 76,
	//VERTEX_RESERVED_6               = 77,
	//VERTEX_RESERVED_7               = 78,
	//MESH_INDICES                    = 79,
	//MESHES                          = 80,
	//MESH_BOUNDS                     = 81,
	//MATERIAL_SORT                   = 82,
	//LIGHTMAP_HEADERS                = 83,
	TWEAK_LIGHTS                    = 85,
	UNKNOWN_97                      = 97,
	//LIGHTMAP_DATA_SKY               = 98,
	//CSM_AABB_NODES                  = 99,
	//CSM_OBJ_REFERENCES              = 100,
	//LIGHTPROBES                     = 101,
	//STATIC_PROP_LIGHTPROBE_INDICES  = 102,
	//LIGHTPROBE_TREE                 = 103,
	//LIGHTPROBE_REFERENCES           = 104,
	//LIGHTMAP_DATA_REAL_TIME_LIGHTS  = 105,
	//CELL_BSP_NODES                  = 106,
	//CELLS                           = 107,
	//PORTALS                         = 108,
	//PORTAL_VERTICES                 = 109,
	//PORTAL_EDGES                    = 110,
	//PORTAL_VERTEX_EDGES             = 111,
	//PORTAL_VERTEX_REFERENCES        = 112,
	//PORTAL_EDGE_REFERENCES          = 113,
	//PORTAL_EDGE_INTERSECT_AT_EDGE   = 114,
	//PORTAL_EDGE_INTERSECT_AT_VERTEX = 115,
	//PORTAL_EDGE_INTERSECT_HEADER     = 116,
	//OCCLUSION_MESH_VERTICES         = 117,
	//OCCLUSION_MESH_INDICES          = 118,
	//CELL_AABB_NODES                 = 119,
	//OBJ_REFERENCES                  = 120,
	//OBJ_REFERENCE_BOUNDS            = 121,
	//LIGHTMAP_DATA_RTL_PAGE          = 122,
	//LEVEL_INFO                      = 123,
	//SHADOW_MESH_OPAQUE_VERTICES     = 124,
	//SHADOW_MESH_ALPHA_VERTICES      = 125,
	//SHADOW_MESH_INDICES             = 126,
	//SHADOW_MESHES                   = 127
};

static const Lump_t apexLegendsLumps[128] = {
	{ "Entities", true, 0 },
	{ "Planes", false, 0 },
	{ "Texture Data", false, 0 },
	{ "Vertices", false, 0 },
	{ "Lightprobe Parent Infos", false, 0 },
	{ "Shadow Environments", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Models", true, 0 },
	{ "Surface Names", true, 0 },
	{ "Contents Masks", false, 0 },
	{ "Surface Properties", false, 0 },
	{ "BVH Nodes", true, 0 },
	{ "BVH Leaf Data", false, 0 },
	{ "Packed Vertices", true, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Entity Partitions", true, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Vertex Normals", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Game Lump", false, 0 },
	{ "Unused", false, 0 },
	{ "Unknown 37", false, 0 },
	{ "Unknown 38", false, 0 },
	{ "Unknown 39", false, 0 },
	{ "PakFile", false, 0 },
	{ "Unused", false, 0 },
	{ "CubeMaps", false, 0 },
	{ "Unknown 43", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "World Lights", false, 0 },
	{ "World Light Parent Infos", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Vertex Unlit", false, 0 },
	{ "Vertex Lit Flat", false, 0 },
	{ "Vertex Lit Bump", false, 0 },
	{ "Vertex Unlit Ts", false, 0 },
	{ "Vertex Blinn Phong", false, 0 },
	{ "Vertex Reserved 5", false, 0 },
	{ "Vertex Reserved 6", false, 0 },
	{ "Vertex Reserved 7", false, 0 },
	{ "Mesh Indices", false, 0 },
	{ "Meshes", true, 0 },
	{ "Mesh Bounds", false, 0 },
	{ "Material Sort", false, 0 },
	{ "Lightmap Headers", false, 0 },
	{ "Unused", false, 0 },
	{ "Tweak Lights", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unused", false, 0 },
	{ "Unknown 97", false, 0 },
	{ "Lightmap Data Sky", false, 0 },
	{ "CSM AABB Nodes", false, 0 },
	{ "CSM Obj References", false, 0 },
	{ "Lightprobes", false, 0 },
	{ "Static Prop Lightprobe Indices", false, 0 },
	{ "Lightprobe Tree", false, 0 },
	{ "Lightprobe References", false, 0 },
	{ "Lightmap Data Real Time Lights", false, 0 },
	{ "Cell BSP Nodes", false, 0 },
	{ "Cells", false, 0 },
	{ "Portals", false, 0 },
	{ "Portal Vertices", false, 0 },
	{ "Portal Edges", false, 0 },
	{ "Portal Vertex Edges", false, 0 },
	{ "Portal Vertex References", false, 0 },
	{ "Portal Edge References", false, 0 },
	{ "Portal Edge Intersect At Edge", false, 0 },
	{ "Portal Edge Intersect At Vertex", false, 0 },
	{ "Portal Edge Intersect Header", false, 0 },
	{ "Occlusion Mesh Vertices", false, 0 },
	{ "Occlusion Mesh Indices", false, 0 },
	{ "Cell AABB Nodes", true, 0 },
	{ "Obj References", true, 0 },
	{ "Obj Reference Bounds", false, 0 },
	{ "Lightmap Data RTL Page", false, 0 },
	{ "Level Info", true, 0 },
	{ "Shadow Mesh Opaque Vertices", false, 0 },
	{ "Shadow Mesh Alpha Vertices", false, 0 },
	{ "Shadow Mesh Indices", false, 0 },
	{ "Shadow Meshes", false, 0 }
};


namespace ApexLegends {
	struct BVHNode_t {
		Vector3h mins0;
		Vector3h mins1;
		Vector3h mins2;
		Vector3h mins3;
		Vector3h maxs0;
		Vector3h maxs1;
		Vector3h maxs2;
		Vector3h maxs3;
		uint32_t unk0;
		uint32_t unk1;
		uint32_t unk2;
		uint32_t unk3;
	};

	struct Model_t {
		AABB aabb;
		uint32_t firstMesh;
		uint32_t meshCount;
		int32_t unknown[8];
	};

	struct VertexLitBump_t {
		uint32_t vertexIndex;
		uint32_t normalIndex;
		float uv0[2];
		uint32_t negativeOne;
		float uv1[2];
		char color[4];
	};

	struct Mesh_t
	{
		uint32_t triangleOffset;
		uint16_t triangleCount;
		uint16_t unknown[8];
		uint16_t materialOffset;
		uint32_t flags;
	};

	struct MaterialSort_t {
		int16_t textureData;
		int16_t lightmapIndex;
		int16_t unknown[2];
		int32_t vertexOffset;
	};

	struct CellAABBNode_t {
		Vector3f mins;
		uint32_t childCount: 8;
		uint32_t childOffset: 16;
		uint32_t childFlags: 8;
		Vector3f maxs;
		uint32_t objRefCount: 8;
		uint32_t objRefOffset: 16;
		uint32_t objRefFlags: 8;
	};

	struct LevelInfo_t {
		// 0x200 + 0x4 - 0x10
		uint32_t unknown[5];
		Vector3f unknownf;
		uint32_t unknown1;
	};
}