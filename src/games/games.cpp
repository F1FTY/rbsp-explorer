// Part of the bsp-viewer project under the MIT license
// Source code & license: https://gitlab.com/F1FTY/rbsp-explorer

#include "games.hpp"


/*
	Name: GetGameTypeFromVersion
	Purpose: Returns eGame based on Bsp.header.version
*/
int GetGameTypeFromVersion( uint32_t version ) {
	switch ( version ) {
		case 29:
			return TITANFALL;
		case 37:
			return TITANFALL2;
		case 47:
			return APEX_LEGENDS;
	};

	return UNSUPPORTED;
}

/*
	Name: GetGameNameFromVersion
	Purpose: Return the name of the corresponding game
*/
std::string GetGameNameFromVersion( int version ) {
	int type = GetGameTypeFromVersion( version );

	switch ( type ) {
		case TITANFALL:
			return "Titanfall";
		case TITANFALL2:
			return "Titanfall 2";
		case APEX_LEGENDS:
			return "Apex Legends";
	};

	return "Unsupported";
}

Lump_t GetLumpInfo( int index, int game ) {
	switch ( game ) {
		case TITANFALL:
			return titanfallLumps[index];
			break;
		case TITANFALL2:
			return titanfall2Lumps[index];
			break;
		case APEX_LEGENDS:
			return apexLegendsLumps[index];
			break;
	};

	Lump_t lump;
	return lump;
}

/*
	Name: IsLumpUsedInGame
	Purpose: Returns true if the specified game uses the lump
*/
bool IsLumpUsedInGame( int index, int game ) {
	switch ( game ) {
		case TITANFALL:
			return IsLumpUsedInTitanfall( index );
		case TITANFALL2:
			return IsLumpUsedInTitanfall2( index );
		case APEX_LEGENDS:
			return IsLumpUsedInApexLegends( index );
	};

	return true;
}

/*
	Name: IsLumpUsedInTitanfall
	Purpose: Returns true if the game uses the lump
*/
bool IsLumpUsedInTitanfall( int index ) {
    if ( index >= 4 && index <= 13 ) return false;
    if ( index >= 15 && index <= 23 ) return false;
    if ( index >= 25 && index <= 28 ) return false;
    if ( index >= 31 && index <= 34 ) return false;
    if ( index >= 37 && index <= 39 ) return false;
    if ( index == 41 ) return false;
    if ( index >= 45 && index <= 53 ) return false;
    if ( index >= 55 && index <= 61 ) return false;
    if ( index >= 63 && index <= 65 ) return false;
    if ( index == 67 ) return false;
    if ( index == 84 ) return false;
    if ( index == 122 ) return false;
    return true;
}

/*
	Name: IsLumpUsedInTitanfall2
	Purpose: Returns true if the game uses the lump
*/
bool IsLumpUsedInTitanfall2( int index ) {
    if ( index >= 8 && index <= 13 ) return false;
    if ( index >= 15 && index <= 23 ) return false;
    if ( index >= 25 && index <= 28 ) return false;
    if ( index >= 31 && index <= 34 ) return false;
    if ( index >= 37 && index <= 39 ) return false;
    if ( index == 41 ) return false;
    if ( index >= 45 && index <= 53 ) return false;
    if ( index >= 56 && index <= 61 ) return false;
    if ( index >= 63 && index <= 65 ) return false;
    if ( index == 67 ) return false;
    if ( index == 84 ) return false;
    return true;
}

/*
	Name: IsLumpUsedInApexLegends
	Purpose: Returns true if the game uses the lump
*/
bool IsLumpUsedInApexLegends( int index ) {
    if ( index >= 6 && index <= 13 ) return false;
    if ( index >= 21 && index <= 23 ) return false;
    if ( index >= 25 && index <= 29 ) return false;
    if ( index >= 31 && index <= 34 ) return false;
    if ( index == 36 ) return false;
    if ( index == 41 ) return false;
    if ( index >= 44 && index <= 53 ) return false;
    if ( index >= 56 && index <= 70 ) return false;
    if ( index == 84 ) return false;
    if ( index >= 86 && index <= 96 ) return false;
    return true;
}
