# rBSP Explorer

An OpenGL + ImGui respawn .bsp explorer.

Although this could be used in map development to inspect maps the main purpose is reverse-engineering the respawn source `.bsp` format. This means this program will only be updated when we advance our understanding of the format. This program will also not be maintained once the format has been reversed to the point of us being able to make feature-complete custom maps.

## References

[bsp_tool](https://github.com/snake-biscuits/bsp_tool)

## Screenshot
![image.png](./screenshot.png)

## Dependencies:
### These you need to most likely download on a fresh system
- GLFW ([Website](https://www.glfw.org/))
- glm ( [Github](https://github.com/g-truc/glm) )
### These are included with the project
- ImGui ([License](./imgui/LICENSE.txt))
# License
```
The MIT License (MIT)

Copyright (c) 2022-2022 Filip Bartoš

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
